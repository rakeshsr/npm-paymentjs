/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { StripeService } from '../vendor/stripe';
import { RazorpayService } from '../vendor/razorpay';
import { PayPalService } from '../vendor/paypal';
var PaymentjsService = /** @class */ (function () {
    function PaymentjsService(stripe, razorpay, paypal) {
        this.stripe = stripe;
        this.razorpay = razorpay;
        this.paypal = paypal;
    }
    /**
     * @param {?} data
     * @return {?}
     */
    PaymentjsService.prototype.checkout = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        var _this = this;
        /** @type {?} */
        var result;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
            var payment_type, _a, e_1;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        payment_type = data.paymentInstrument.toLowerCase();
                        _b.label = 1;
                    case 1:
                        _b.trys.push([1, 10, , 11]);
                        _a = payment_type;
                        switch (_a) {
                            case 'payment_stripe': return [3 /*break*/, 2];
                            case 'payment_razorpay': return [3 /*break*/, 4];
                            case 'payment_paypal': return [3 /*break*/, 6];
                        }
                        return [3 /*break*/, 8];
                    case 2: return [4 /*yield*/, this.stripe.charge(data.params)];
                    case 3:
                        result = _b.sent();
                        return [3 /*break*/, 9];
                    case 4: return [4 /*yield*/, this.razorpay.charge(data.params)];
                    case 5:
                        result = _b.sent();
                        return [3 /*break*/, 9];
                    case 6: return [4 /*yield*/, this.paypal.charge(data.params)];
                    case 7:
                        result = _b.sent();
                        return [3 /*break*/, 9];
                    case 8:
                        reject("Unsupported Payment Instrument " + data.paymentInstrument);
                        return [2 /*return*/, false];
                    case 9:
                        result["isTest"] = data.params.isTest;
                        result["paymentInstrument"] = data.paymentInstrument;
                        resolve(result);
                        return [3 /*break*/, 11];
                    case 10:
                        e_1 = _b.sent();
                        console.log(e_1);
                        reject('Transaction cancelled!');
                        return [3 /*break*/, 11];
                    case 11: return [2 /*return*/];
                }
            });
        }); }));
    };
    PaymentjsService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    PaymentjsService.ctorParameters = function () { return [
        { type: StripeService },
        { type: RazorpayService },
        { type: PayPalService }
    ]; };
    return PaymentjsService;
}());
export { PaymentjsService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    PaymentjsService.prototype.stripe;
    /**
     * @type {?}
     * @private
     */
    PaymentjsService.prototype.razorpay;
    /**
     * @type {?}
     * @private
     */
    PaymentjsService.prototype.paypal;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGF5bWVudGpzLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9wYXltZW50anMvIiwic291cmNlcyI6WyJsaWIvcGF5bWVudGpzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDckQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRWpEO0lBRUUsMEJBQ1UsTUFBcUIsRUFDckIsUUFBeUIsRUFDekIsTUFBcUI7UUFGckIsV0FBTSxHQUFOLE1BQU0sQ0FBZTtRQUNyQixhQUFRLEdBQVIsUUFBUSxDQUFpQjtRQUN6QixXQUFNLEdBQU4sTUFBTSxDQUFlO0lBQzNCLENBQUM7Ozs7O0lBRUwsbUNBQVE7Ozs7SUFBUixVQUFTLElBQVM7UUFBbEIsaUJBK0JDOztZQTlCSyxNQUFXO1FBQ2YsT0FBTyxJQUFJLE9BQU87Ozs7O1FBQUMsVUFBTSxPQUFPLEVBQUUsTUFBTTs7Ozs7d0JBQ2hDLFlBQVksR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxFQUFFOzs7O3dCQUVoRCxLQUFBLFlBQVksQ0FBQTs7aUNBQ1osZ0JBQWdCLENBQUMsQ0FBakIsd0JBQWdCO2lDQUloQixrQkFBa0IsQ0FBQyxDQUFuQix3QkFBa0I7aUNBSWxCLGdCQUFnQixDQUFDLENBQWpCLHdCQUFnQjs7OzRCQVBWLHFCQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBQTs7d0JBQTlDLE1BQU0sR0FBRyxTQUFxQyxDQUFDO3dCQUMvQyx3QkFBTTs0QkFHRyxxQkFBTSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUE7O3dCQUFoRCxNQUFNLEdBQUcsU0FBdUMsQ0FBQzt3QkFDakQsd0JBQU07NEJBR0cscUJBQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFBOzt3QkFBOUMsTUFBTSxHQUFHLFNBQXFDLENBQUM7d0JBQy9DLHdCQUFNOzt3QkFHTixNQUFNLENBQUMsb0NBQWtDLElBQUksQ0FBQyxpQkFBbUIsQ0FBQyxDQUFDO3dCQUNuRSxzQkFBTyxLQUFLLEVBQUM7O3dCQUdqQixNQUFNLENBQUMsUUFBUSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUM7d0JBQ3RDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQzt3QkFDckQsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDOzs7O3dCQUVoQixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUMsQ0FBQyxDQUFDO3dCQUNmLE1BQU0sQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDOzs7OzthQUVwQyxFQUFDLENBQUE7SUFDSixDQUFDOztnQkF2Q0YsVUFBVTs7OztnQkFKRixhQUFhO2dCQUNiLGVBQWU7Z0JBQ2YsYUFBYTs7SUEwQ3RCLHVCQUFDO0NBQUEsQUF4Q0QsSUF3Q0M7U0F2Q1ksZ0JBQWdCOzs7Ozs7SUFFekIsa0NBQTZCOzs7OztJQUM3QixvQ0FBaUM7Ozs7O0lBQ2pDLGtDQUE2QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFN0cmlwZVNlcnZpY2UgfSBmcm9tICcuLi92ZW5kb3Ivc3RyaXBlJztcbmltcG9ydCB7IFJhem9ycGF5U2VydmljZSB9IGZyb20gJy4uL3ZlbmRvci9yYXpvcnBheSc7XG5pbXBvcnQgeyBQYXlQYWxTZXJ2aWNlIH0gZnJvbSAnLi4vdmVuZG9yL3BheXBhbCc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBQYXltZW50anNTZXJ2aWNlIHtcbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBzdHJpcGU6IFN0cmlwZVNlcnZpY2UsXG4gICAgcHJpdmF0ZSByYXpvcnBheTogUmF6b3JwYXlTZXJ2aWNlLFxuICAgIHByaXZhdGUgcGF5cGFsOiBQYXlQYWxTZXJ2aWNlLFxuICApIHsgfVxuICBcbiAgY2hlY2tvdXQoZGF0YTogYW55KXtcbiAgICBsZXQgcmVzdWx0OiBhbnk7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKGFzeW5jKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgY29uc3QgcGF5bWVudF90eXBlID0gZGF0YS5wYXltZW50SW5zdHJ1bWVudC50b0xvd2VyQ2FzZSgpO1xuICAgICAgdHJ5e1xuICAgICAgICBzd2l0Y2gocGF5bWVudF90eXBlKXtcbiAgICAgICAgICBjYXNlICdwYXltZW50X3N0cmlwZSc6XG4gICAgICAgICAgICByZXN1bHQgPSBhd2FpdCB0aGlzLnN0cmlwZS5jaGFyZ2UoZGF0YS5wYXJhbXMpO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgXG4gICAgICAgICAgY2FzZSAncGF5bWVudF9yYXpvcnBheSc6XG4gICAgICAgICAgICByZXN1bHQgPSBhd2FpdCB0aGlzLnJhem9ycGF5LmNoYXJnZShkYXRhLnBhcmFtcyk7XG4gICAgICAgICAgICBicmVhaztcbiAgICBcbiAgICAgICAgICBjYXNlICdwYXltZW50X3BheXBhbCc6XG4gICAgICAgICAgICByZXN1bHQgPSBhd2FpdCB0aGlzLnBheXBhbC5jaGFyZ2UoZGF0YS5wYXJhbXMpO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgXG4gICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgIHJlamVjdChgVW5zdXBwb3J0ZWQgUGF5bWVudCBJbnN0cnVtZW50ICR7ZGF0YS5wYXltZW50SW5zdHJ1bWVudH1gKTtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICAgIHJlc3VsdFtcImlzVGVzdFwiXSA9IGRhdGEucGFyYW1zLmlzVGVzdDtcbiAgICAgICAgcmVzdWx0W1wicGF5bWVudEluc3RydW1lbnRcIl0gPSBkYXRhLnBheW1lbnRJbnN0cnVtZW50O1xuICAgICAgICByZXNvbHZlKHJlc3VsdCk7IFxuICAgICAgfWNhdGNoKGUpe1xuICAgICAgICBjb25zb2xlLmxvZyhlKTtcbiAgICAgICAgcmVqZWN0KCdUcmFuc2FjdGlvbiBjYW5jZWxsZWQhJyk7XG4gICAgICB9XG4gICAgfSkgIFxuICB9XG59XG4iXX0=