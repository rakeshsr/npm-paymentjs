/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { v4 as uuid } from 'uuid';
import 'url-search-params-polyfill';
var StripeService = /** @class */ (function () {
    function StripeService(http) {
        this.http = http;
        this.addScript = false;
        this.scriptReady = true;
        this.__init();
    }
    /**
     * @return {?}
     */
    StripeService.prototype.__init = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (!this.addScript) {
            this.injectScript().then((/**
             * @return {?}
             */
            function () {
                _this.scriptReady = false;
                console.log('stripe.js script ready', window.StripeCheckout);
            }));
        }
    };
    /**
     * @return {?}
     */
    StripeService.prototype.injectScript = /**
     * @return {?}
     */
    function () {
        this.addScript = true;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            /** @type {?} */
            var scripttagElement = document.createElement('script');
            scripttagElement.src = 'https://checkout.stripe.com/checkout.js';
            scripttagElement.onload = resolve;
            scripttagElement.onerror = reject;
            document.body.appendChild(scripttagElement);
        }));
    };
    /**
     * @param {?} params
     * @return {?}
     */
    StripeService.prototype.fakeCharge = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            /** @type {?} */
            var total_amount = parseInt(params["amount"]);
            /** @type {?} */
            var fee_amount = (((100 * 2.9) / total_amount) + 0.30).toFixed(2);
            /** @type {?} */
            var net_amount = (total_amount - fee_amount).toFixed(2);
            resolve({
                "trxn_id": "ch_" + uuid(),
                "total_amount": total_amount,
                "fee_amount": fee_amount,
                "net_amount": net_amount,
                "currency": "usd"
            });
        }));
    };
    /**
     * @param {?} params
     * @return {?}
     */
    StripeService.prototype.charge = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        /** @type {?} */
        var self = this;
        params.amount = (params.amount || 0) * 100;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            self.handler = window.StripeCheckout.configure({
                key: params.username,
                locale: 'auto',
                name: params.name || '',
                image: params.image || '',
                billingAddress: false,
                zipCode: false,
                allowRememberMe: false,
                description: params.description || '',
                closed: (/**
                 * @param {?} e
                 * @return {?}
                 */
                function (e) {
                    if (!self.handler.isTokenGenerate) {
                        reject({
                            "code": "stripe_transaction_cancelled"
                        });
                    }
                }),
                token: (/**
                 * @param {?} token
                 * @return {?}
                 */
                function (token) {
                    resolve({
                        "total_amount": (params.amount / 100),
                        "fee_amount": 0,
                        "net_amount": (params.amount / 100),
                        "currency": params.currency,
                        "trxn_id": token.id
                    });
                    return false;
                })
            });
            self.handler.open(params);
        }));
    };
    /**
     * @return {?}
     */
    StripeService.prototype.onPopstate = /**
     * @return {?}
     */
    function () {
        this.handler.close();
    };
    StripeService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    StripeService.ctorParameters = function () { return [
        { type: HttpClient }
    ]; };
    StripeService.propDecorators = {
        onPopstate: [{ type: HostListener, args: ['window:popstate',] }]
    };
    return StripeService;
}());
export { StripeService };
if (false) {
    /** @type {?} */
    StripeService.prototype.addScript;
    /** @type {?} */
    StripeService.prototype.scriptReady;
    /** @type {?} */
    StripeService.prototype.handler;
    /**
     * @type {?}
     * @private
     */
    StripeService.prototype.http;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RyaXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vcGF5bWVudGpzLyIsInNvdXJjZXMiOlsidmVuZG9yL3N0cmlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekQsT0FBTyxFQUFFLFVBQVUsRUFBZSxNQUFNLHNCQUFzQixDQUFDO0FBQy9ELE9BQU8sRUFBRSxFQUFFLElBQUksSUFBSSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2xDLE9BQU8sNEJBQTRCLENBQUE7QUFHbkM7SUFLRSx1QkFBb0IsSUFBZ0I7UUFBaEIsU0FBSSxHQUFKLElBQUksQ0FBWTtRQUhwQyxjQUFTLEdBQVksS0FBSyxDQUFDO1FBQzNCLGdCQUFXLEdBQVksSUFBSSxDQUFDO1FBRVksSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQUMsQ0FBQzs7OztJQUV4RCw4QkFBTTs7O0lBQU47UUFBQSxpQkFPQztRQU5DLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ25CLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQyxJQUFJOzs7WUFBQztnQkFDdkIsS0FBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7Z0JBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLEVBQUUsTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQy9ELENBQUMsRUFBQyxDQUFBO1NBQ0g7SUFDSCxDQUFDOzs7O0lBRUQsb0NBQVk7OztJQUFaO1FBQ0UsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDdEIsT0FBTyxJQUFJLE9BQU87Ozs7O1FBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTs7Z0JBQzdCLGdCQUFnQixHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDO1lBQ3ZELGdCQUFnQixDQUFDLEdBQUcsR0FBRyx5Q0FBeUMsQ0FBQztZQUNqRSxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDO1lBQ2xDLGdCQUFnQixDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7WUFDbEMsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUM5QyxDQUFDLEVBQUMsQ0FBQTtJQUNKLENBQUM7Ozs7O0lBRUQsa0NBQVU7Ozs7SUFBVixVQUFXLE1BQVc7UUFDcEIsT0FBTyxJQUFJLE9BQU87Ozs7O1FBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTs7Z0JBQzdCLFlBQVksR0FBTyxRQUFRLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDOztnQkFDN0MsVUFBVSxHQUFPLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsR0FBRyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDOztnQkFDakUsVUFBVSxHQUFPLENBQUMsWUFBWSxHQUFHLFVBQVUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFFM0QsT0FBTyxDQUFDO2dCQUNOLFNBQVMsRUFBRSxLQUFLLEdBQUcsSUFBSSxFQUFFO2dCQUN6QixjQUFjLEVBQUUsWUFBWTtnQkFDNUIsWUFBWSxFQUFFLFVBQVU7Z0JBQ3hCLFlBQVksRUFBRSxVQUFVO2dCQUN4QixVQUFVLEVBQUUsS0FBSzthQUNsQixDQUFDLENBQUM7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsOEJBQU07Ozs7SUFBTixVQUFPLE1BQVc7O1lBQ1osSUFBSSxHQUFRLElBQUk7UUFDcEIsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO1FBQzNDLE9BQU8sSUFBSSxPQUFPOzs7OztRQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFDakMsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQztnQkFDN0MsR0FBRyxFQUFFLE1BQU0sQ0FBQyxRQUFRO2dCQUNwQixNQUFNLEVBQUUsTUFBTTtnQkFDZCxJQUFJLEVBQUUsTUFBTSxDQUFDLElBQUksSUFBSSxFQUFFO2dCQUN2QixLQUFLLEVBQUUsTUFBTSxDQUFDLEtBQUssSUFBSSxFQUFFO2dCQUN6QixjQUFjLEVBQUUsS0FBSztnQkFDckIsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsZUFBZSxFQUFFLEtBQUs7Z0JBQ3RCLFdBQVcsRUFBRSxNQUFNLENBQUMsV0FBVyxJQUFJLEVBQUU7Z0JBQ3JDLE1BQU07Ozs7Z0JBQUUsVUFBVSxDQUFDO29CQUNqQixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLEVBQUU7d0JBQ2pDLE1BQU0sQ0FBQzs0QkFDTCxNQUFNLEVBQUUsOEJBQThCO3lCQUN2QyxDQUFDLENBQUM7cUJBQ0o7Z0JBQ0gsQ0FBQyxDQUFBO2dCQUNELEtBQUs7Ozs7Z0JBQUUsVUFBUyxLQUFLO29CQUNuQixPQUFPLENBQUM7d0JBQ04sY0FBYyxFQUFFLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUM7d0JBQ3JDLFlBQVksRUFBQyxDQUFDO3dCQUNkLFlBQVksRUFBRSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDO3dCQUNuQyxVQUFVLEVBQUUsTUFBTSxDQUFDLFFBQVE7d0JBQzNCLFNBQVMsRUFBRSxLQUFLLENBQUMsRUFBRTtxQkFDcEIsQ0FBQyxDQUFDO29CQUNILE9BQU8sS0FBSyxDQUFDO2dCQUNmLENBQUMsQ0FBQTthQUNGLENBQUMsQ0FBQztZQUVILElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzVCLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUdELGtDQUFVOzs7SUFEVjtRQUVFLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUE7SUFDdEIsQ0FBQzs7Z0JBbEZGLFVBQVU7Ozs7Z0JBTEYsVUFBVTs7OzZCQW9GaEIsWUFBWSxTQUFDLGlCQUFpQjs7SUFJakMsb0JBQUM7Q0FBQSxBQW5GRCxJQW1GQztTQWxGWSxhQUFhOzs7SUFDeEIsa0NBQTJCOztJQUMzQixvQ0FBNEI7O0lBQzVCLGdDQUFhOzs7OztJQUNELDZCQUF3QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIEhvc3RMaXN0ZW5lciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cENsaWVudCwgSHR0cEhlYWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyB2NCBhcyB1dWlkIH0gZnJvbSAndXVpZCc7XG5pbXBvcnQgJ3VybC1zZWFyY2gtcGFyYW1zLXBvbHlmaWxsJ1xuZGVjbGFyZSB2YXIgd2luZG93OiBhbnk7XG7igItcbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBTdHJpcGVTZXJ2aWNlIHtcbiAgYWRkU2NyaXB0OiBib29sZWFuID0gZmFsc2U7XG4gIHNjcmlwdFJlYWR5OiBib29sZWFuID0gdHJ1ZTtcbiAgaGFuZGxlcjogYW55O1xuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQpIHsgdGhpcy5fX2luaXQoKTsgfVxu4oCLXG4gIF9faW5pdCgpOiB2b2lkIHtcbiAgICBpZiAoIXRoaXMuYWRkU2NyaXB0KSB7XG4gICAgICB0aGlzLmluamVjdFNjcmlwdCgpLnRoZW4oKCkgPT4ge1xuICAgICAgICB0aGlzLnNjcmlwdFJlYWR5ID0gZmFsc2U7XG4gICAgICAgIGNvbnNvbGUubG9nKCdzdHJpcGUuanMgc2NyaXB0IHJlYWR5Jywgd2luZG93LlN0cmlwZUNoZWNrb3V0KTtcbiAgICAgIH0pXG4gICAgfVxuICB9XG5cbiAgaW5qZWN0U2NyaXB0KCkge1xuICAgIHRoaXMuYWRkU2NyaXB0ID0gdHJ1ZTtcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgbGV0IHNjcmlwdHRhZ0VsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzY3JpcHQnKTtcbiAgICAgIHNjcmlwdHRhZ0VsZW1lbnQuc3JjID0gJ2h0dHBzOi8vY2hlY2tvdXQuc3RyaXBlLmNvbS9jaGVja291dC5qcyc7XG4gICAgICBzY3JpcHR0YWdFbGVtZW50Lm9ubG9hZCA9IHJlc29sdmU7XG4gICAgICBzY3JpcHR0YWdFbGVtZW50Lm9uZXJyb3IgPSByZWplY3Q7XG4gICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKHNjcmlwdHRhZ0VsZW1lbnQpO1xuICAgIH0pXG4gIH1cblxuICBmYWtlQ2hhcmdlKHBhcmFtczogYW55KSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIGxldCB0b3RhbF9hbW91bnQ6YW55ID0gcGFyc2VJbnQocGFyYW1zW1wiYW1vdW50XCJdKTtcbiAgICAgIGxldCBmZWVfYW1vdW50OmFueSA9ICgoKDEwMCAqIDIuOSkgLyB0b3RhbF9hbW91bnQpICsgMC4zMCkudG9GaXhlZCgyKTtcbiAgICAgIGxldCBuZXRfYW1vdW50OmFueSA9ICh0b3RhbF9hbW91bnQgLSBmZWVfYW1vdW50KS50b0ZpeGVkKDIpO1xu4oCLXG4gICAgICByZXNvbHZlKHtcbiAgICAgICAgXCJ0cnhuX2lkXCI6IFwiY2hfXCIgKyB1dWlkKCksXG4gICAgICAgIFwidG90YWxfYW1vdW50XCI6IHRvdGFsX2Ftb3VudCxcbiAgICAgICAgXCJmZWVfYW1vdW50XCI6IGZlZV9hbW91bnQsXG4gICAgICAgIFwibmV0X2Ftb3VudFwiOiBuZXRfYW1vdW50LFxuICAgICAgICBcImN1cnJlbmN5XCI6IFwidXNkXCJcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG7igItcbiAgY2hhcmdlKHBhcmFtczogYW55KSB7XG4gICAgbGV0IHNlbGY6IGFueSA9IHRoaXM7XG4gICAgcGFyYW1zLmFtb3VudCA9IChwYXJhbXMuYW1vdW50IHx8IDApICogMTAwO1xuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBzZWxmLmhhbmRsZXIgPSB3aW5kb3cuU3RyaXBlQ2hlY2tvdXQuY29uZmlndXJlKHtcbiAgICAgICAga2V5OiBwYXJhbXMudXNlcm5hbWUsXG4gICAgICAgIGxvY2FsZTogJ2F1dG8nLFxuICAgICAgICBuYW1lOiBwYXJhbXMubmFtZSB8fCAnJyxcbiAgICAgICAgaW1hZ2U6IHBhcmFtcy5pbWFnZSB8fCAnJyxcbiAgICAgICAgYmlsbGluZ0FkZHJlc3M6IGZhbHNlLFxuICAgICAgICB6aXBDb2RlOiBmYWxzZSxcbiAgICAgICAgYWxsb3dSZW1lbWJlck1lOiBmYWxzZSxcbiAgICAgICAgZGVzY3JpcHRpb246IHBhcmFtcy5kZXNjcmlwdGlvbiB8fCAnJyxcbiAgICAgICAgY2xvc2VkOiBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgIGlmICghc2VsZi5oYW5kbGVyLmlzVG9rZW5HZW5lcmF0ZSkge1xuICAgICAgICAgICAgcmVqZWN0KHtcbiAgICAgICAgICAgICAgXCJjb2RlXCI6IFwic3RyaXBlX3RyYW5zYWN0aW9uX2NhbmNlbGxlZFwiXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHRva2VuOiBmdW5jdGlvbih0b2tlbikge1xuICAgICAgICAgIHJlc29sdmUoe1xuICAgICAgICAgICAgXCJ0b3RhbF9hbW91bnRcIjogKHBhcmFtcy5hbW91bnQgLyAxMDApLFxuICAgICAgICAgICAgXCJmZWVfYW1vdW50XCI6MCxcbiAgICAgICAgICAgIFwibmV0X2Ftb3VudFwiOiAocGFyYW1zLmFtb3VudCAvIDEwMCksXG4gICAgICAgICAgICBcImN1cnJlbmN5XCI6IHBhcmFtcy5jdXJyZW5jeSxcbiAgICAgICAgICAgIFwidHJ4bl9pZFwiOiB0b2tlbi5pZFxuICAgICAgICAgIH0pO1xuICAgICAgICAgIHJldHVybiBmYWxzZTsgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgIH0pO1xu4oCLXG4gICAgICBzZWxmLmhhbmRsZXIub3BlbihwYXJhbXMpO1xuICAgIH0pO1xuICB9XG7igItcbiAgQEhvc3RMaXN0ZW5lcignd2luZG93OnBvcHN0YXRlJylcbiAgb25Qb3BzdGF0ZSgpIHtcbiAgICB0aGlzLmhhbmRsZXIuY2xvc2UoKVxuICB9XG59Il19