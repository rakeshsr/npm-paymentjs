/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { v4 as uuid } from 'uuid';
import { PaypalModalComponent } from './paypal.component';
import { NgxSmartModalService } from 'ngx-smart-modal';
import 'url-search-params-polyfill';
var PayPalService = /** @class */ (function () {
    function PayPalService(modalService) {
        this.modalService = modalService;
        this.addScript = false;
        this.scriptReady = true;
        this.__init();
    }
    /**
     * @return {?}
     */
    PayPalService.prototype.__init = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (!this.addScript) {
            this.injectScript().then((/**
             * @return {?}
             */
            function () {
                _this.scriptReady = false;
                console.log('PayPal checkout.js script ready');
            }));
        }
    };
    /**
     * @param {?} params
     * @param {?} resolve
     * @param {?} reject
     * @return {?}
     */
    PayPalService.prototype.config = /**
     * @param {?} params
     * @param {?} resolve
     * @param {?} reject
     * @return {?}
     */
    function (params, resolve, reject) {
        var _this = this;
        return {
            "locale": 'en_US',
            "env": params.isTest ? 'sandbox' : 'production',
            "style": {
                "layout": 'vertical',
            },
            "client": {
                "sandbox": params.username,
                "production": params.username,
            },
            "commit": true,
            "payment": (/**
             * @param {?} data
             * @param {?} actions
             * @return {?}
             */
            function (data, actions) {
                return actions.payment.create({
                    "payment": {
                        "transactions": [
                            {
                                "amount": {
                                    "total": params.amount,
                                    "currency": params.currency
                                },
                                "description": params.description || ''
                            },
                        ],
                    },
                });
            }),
            onAuthorize: (/**
             * @param {?} data
             * @param {?} actions
             * @return {?}
             */
            function (data, actions) {
                return actions.payment
                    .execute()
                    .then((/**
                 * @param {?} payment
                 * @return {?}
                 */
                function (payment) {
                    /** @type {?} */
                    var transaction_id = '';
                    if (payment &&
                        payment.transactions &&
                        payment.transactions.length &&
                        payment.transactions[0].related_resources &&
                        payment.transactions[0].related_resources.length &&
                        payment.transactions[0].related_resources[0].sale &&
                        payment.transactions[0].related_resources[0].sale.id) {
                        transaction_id =
                            payment.transactions[0].related_resources[0].sale.id;
                    }
                    // check if the PayPal transaction went through
                    if (payment.intent != 'sale' && payment.state != 'approved') {
                        reject(payment);
                    }
                    else {
                        resolve({
                            total_amount: params.amount,
                            fee_amount: 0,
                            net_amount: params.amount,
                            currency: params.currency,
                            trxn_id: transaction_id,
                        });
                        if (_this.modalRef && _this.modalRef.close) {
                            _this.modalRef.close();
                        }
                    }
                }))
                    .catch((/**
                 * @param {?} e
                 * @return {?}
                 */
                function (e) {
                    console.log(e);
                    reject(null);
                }));
            }),
        };
    };
    /**
     * @return {?}
     */
    PayPalService.prototype.injectScript = /**
     * @return {?}
     */
    function () {
        this.addScript = true;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            /** @type {?} */
            var scripttagElement = document.createElement('script');
            scripttagElement.src = 'https://www.paypalobjects.com/api/checkout.js';
            scripttagElement.onload = resolve;
            scripttagElement.onerror = reject;
            document.body.appendChild(scripttagElement);
        }));
    };
    /**
     * @param {?} params
     * @return {?}
     */
    PayPalService.prototype.fakeCharge = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            /** @type {?} */
            var total_amount = parseInt(params['amount']);
            /** @type {?} */
            var fee_amount = ((100 * 2.9) / total_amount + 0.3).toFixed(2);
            /** @type {?} */
            var net_amount = (total_amount - fee_amount).toFixed(2);
            resolve({
                trxn_id: 'ch_' + uuid(),
                total_amount: total_amount,
                fee_amount: fee_amount,
                net_amount: net_amount,
                currency: 'usd',
            });
        }));
    };
    /**
     * @param {?} params
     * @return {?}
     */
    PayPalService.prototype.charge = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        var _this = this;
        /** @type {?} */
        var self = this;
        params.amount = params.amount || 0;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            if (!_this.addScript) {
                console.error('PayPal checkout.js script is not ready');
                return false;
            }
            /** @type {?} */
            var modalOptions = {
                escapable: false,
                customClass: 'paypalModalClass'
            };
            _this.modalRef = _this.modalService.create('PaypalModal', PaypalModalComponent, modalOptions).open();
            _this.modalRef.onOpenFinished.subscribe((/**
             * @param {?} result
             * @return {?}
             */
            function (result) {
                _this.modalService.setModalData({ isTest: params.isTest }, 'PaypalModal');
            }));
            _this.modalRef.onClose.subscribe((/**
             * @param {?} result
             * @return {?}
             */
            function (result) {
                console.log('closed', result);
            }));
            window.setTimeout((/**
             * @return {?}
             */
            function () {
                window.paypal.Button.render(self.config(params, resolve, reject), '#paypal-buttons-container');
            }), 1000);
        }));
    };
    PayPalService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    PayPalService.ctorParameters = function () { return [
        { type: NgxSmartModalService }
    ]; };
    return PayPalService;
}());
export { PayPalService };
if (false) {
    /** @type {?} */
    PayPalService.prototype.addScript;
    /** @type {?} */
    PayPalService.prototype.scriptReady;
    /** @type {?} */
    PayPalService.prototype.handler;
    /**
     * @type {?}
     * @private
     */
    PayPalService.prototype.modalRef;
    /**
     * @type {?}
     * @private
     */
    PayPalService.prototype.modalService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGF5cGFsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vcGF5bWVudGpzLyIsInNvdXJjZXMiOlsidmVuZG9yL3BheXBhbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUMsRUFBRSxJQUFJLElBQUksRUFBQyxNQUFNLE1BQU0sQ0FBQztBQUNoQyxPQUFPLEVBQUMsb0JBQW9CLEVBQUMsTUFBTSxvQkFBb0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUV2RCxPQUFPLDRCQUE0QixDQUFDO0FBRXBDO0lBT0UsdUJBQW9CLFlBQWtDO1FBQWxDLGlCQUFZLEdBQVosWUFBWSxDQUFzQjtRQUx0RCxjQUFTLEdBQVksS0FBSyxDQUFDO1FBQzNCLGdCQUFXLEdBQVksSUFBSSxDQUFDO1FBSzFCLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUNoQixDQUFDOzs7O0lBQ0QsOEJBQU07OztJQUFOO1FBQUEsaUJBT0M7UUFOQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNuQixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsSUFBSTs7O1lBQUM7Z0JBQ3ZCLEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO2dCQUN6QixPQUFPLENBQUMsR0FBRyxDQUFDLGlDQUFpQyxDQUFDLENBQUM7WUFDakQsQ0FBQyxFQUFDLENBQUM7U0FDSjtJQUNILENBQUM7Ozs7Ozs7SUFFRCw4QkFBTTs7Ozs7O0lBQU4sVUFBTyxNQUFNLEVBQUUsT0FBTyxFQUFFLE1BQU07UUFBOUIsaUJBbUVDO1FBbEVDLE9BQU87WUFDTCxRQUFRLEVBQUUsT0FBTztZQUNqQixLQUFLLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxZQUFZO1lBQy9DLE9BQU8sRUFBRTtnQkFDUCxRQUFRLEVBQUUsVUFBVTthQUNyQjtZQUNELFFBQVEsRUFBRTtnQkFDUixTQUFTLEVBQUUsTUFBTSxDQUFDLFFBQVE7Z0JBQzFCLFlBQVksRUFBRSxNQUFNLENBQUMsUUFBUTthQUM5QjtZQUNELFFBQVEsRUFBRSxJQUFJO1lBQ2QsU0FBUzs7Ozs7WUFBRSxVQUFDLElBQUksRUFBRSxPQUFPO2dCQUN2QixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDO29CQUM1QixTQUFTLEVBQUU7d0JBQ1QsY0FBYyxFQUFFOzRCQUNkO2dDQUNFLFFBQVEsRUFBRTtvQ0FDUixPQUFPLEVBQUUsTUFBTSxDQUFDLE1BQU07b0NBQ3RCLFVBQVUsRUFBRSxNQUFNLENBQUMsUUFBUTtpQ0FDNUI7Z0NBQ0QsYUFBYSxFQUFFLE1BQU0sQ0FBQyxXQUFXLElBQUksRUFBRTs2QkFDeEM7eUJBQ0Y7cUJBQ0Y7aUJBQ0YsQ0FBQyxDQUFDO1lBQ0wsQ0FBQyxDQUFBO1lBQ0QsV0FBVzs7Ozs7WUFBRSxVQUFDLElBQUksRUFBRSxPQUFPO2dCQUN6QixPQUFPLE9BQU8sQ0FBQyxPQUFPO3FCQUNuQixPQUFPLEVBQUU7cUJBQ1QsSUFBSTs7OztnQkFBQyxVQUFBLE9BQU87O3dCQUNQLGNBQWMsR0FBRyxFQUFFO29CQUN2QixJQUNFLE9BQU87d0JBQ1AsT0FBTyxDQUFDLFlBQVk7d0JBQ3BCLE9BQU8sQ0FBQyxZQUFZLENBQUMsTUFBTTt3QkFDM0IsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUI7d0JBQ3pDLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsTUFBTTt3QkFDaEQsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJO3dCQUNqRCxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQ3BEO3dCQUNBLGNBQWM7NEJBQ1osT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO3FCQUN4RDtvQkFDRCwrQ0FBK0M7b0JBQy9DLElBQUksT0FBTyxDQUFDLE1BQU0sSUFBSSxNQUFNLElBQUksT0FBTyxDQUFDLEtBQUssSUFBSSxVQUFVLEVBQUU7d0JBQzNELE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztxQkFDakI7eUJBQU07d0JBQ0wsT0FBTyxDQUFDOzRCQUNOLFlBQVksRUFBRSxNQUFNLENBQUMsTUFBTTs0QkFDM0IsVUFBVSxFQUFFLENBQUM7NEJBQ2IsVUFBVSxFQUFFLE1BQU0sQ0FBQyxNQUFNOzRCQUN6QixRQUFRLEVBQUUsTUFBTSxDQUFDLFFBQVE7NEJBQ3pCLE9BQU8sRUFBRSxjQUFjO3lCQUN4QixDQUFDLENBQUM7d0JBRUgsSUFBRyxLQUFJLENBQUMsUUFBUSxJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFDOzRCQUN0QyxLQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDO3lCQUN2QjtxQkFDRjtnQkFDSCxDQUFDLEVBQUM7cUJBQ0QsS0FBSzs7OztnQkFBQyxVQUFBLENBQUM7b0JBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDZixNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2YsQ0FBQyxFQUFDLENBQUM7WUFDUCxDQUFDLENBQUE7U0FDRixDQUFDO0lBQ0osQ0FBQzs7OztJQUVELG9DQUFZOzs7SUFBWjtRQUNFLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLE9BQU8sSUFBSSxPQUFPOzs7OztRQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07O2dCQUM3QixnQkFBZ0IsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQztZQUN2RCxnQkFBZ0IsQ0FBQyxHQUFHLEdBQUcsK0NBQStDLENBQUM7WUFDdkUsZ0JBQWdCLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQztZQUNsQyxnQkFBZ0IsQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1lBQ2xDLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDOUMsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUVELGtDQUFVOzs7O0lBQVYsVUFBVyxNQUFXO1FBQ3BCLE9BQU8sSUFBSSxPQUFPOzs7OztRQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07O2dCQUM3QixZQUFZLEdBQVEsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQzs7Z0JBQzlDLFVBQVUsR0FBUSxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxHQUFHLFlBQVksR0FBRyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDOztnQkFDL0QsVUFBVSxHQUFRLENBQUMsWUFBWSxHQUFHLFVBQVUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDNUQsT0FBTyxDQUFDO2dCQUNOLE9BQU8sRUFBRSxLQUFLLEdBQUcsSUFBSSxFQUFFO2dCQUN2QixZQUFZLEVBQUUsWUFBWTtnQkFDMUIsVUFBVSxFQUFFLFVBQVU7Z0JBQ3RCLFVBQVUsRUFBRSxVQUFVO2dCQUN0QixRQUFRLEVBQUUsS0FBSzthQUNoQixDQUFDLENBQUM7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsOEJBQU07Ozs7SUFBTixVQUFPLE1BQVc7UUFBbEIsaUJBOEJDOztZQTdCSyxJQUFJLEdBQVEsSUFBSTtRQUNwQixNQUFNLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDO1FBQ25DLE9BQU8sSUFBSSxPQUFPOzs7OztRQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFDakMsSUFBSSxDQUFDLEtBQUksQ0FBQyxTQUFTLEVBQUU7Z0JBQ25CLE9BQU8sQ0FBQyxLQUFLLENBQUMsd0NBQXdDLENBQUMsQ0FBQztnQkFDeEQsT0FBTyxLQUFLLENBQUM7YUFDZDs7Z0JBRUssWUFBWSxHQUFRO2dCQUN4QixTQUFTLEVBQUUsS0FBSztnQkFDaEIsV0FBVyxFQUFFLGtCQUFrQjthQUNoQztZQUVELEtBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLG9CQUFvQixFQUFFLFlBQVksQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1lBQ25HLEtBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLFNBQVM7Ozs7WUFBQyxVQUFDLE1BQU07Z0JBQzVDLEtBQUksQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLEVBQUUsTUFBTSxFQUFFLE1BQU0sQ0FBQyxNQUFNLEVBQUUsRUFBRSxhQUFhLENBQUMsQ0FBQztZQUMzRSxDQUFDLEVBQUMsQ0FBQztZQUVILEtBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFNBQVM7Ozs7WUFBQyxVQUFDLE1BQU07Z0JBQ3JDLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFBO1lBQy9CLENBQUMsRUFBQyxDQUFDO1lBRUgsTUFBTSxDQUFDLFVBQVU7OztZQUFDO2dCQUNoQixNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQ3pCLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE9BQU8sRUFBRSxNQUFNLENBQUMsRUFDcEMsMkJBQTJCLENBQzVCLENBQUM7WUFDSixDQUFDLEdBQUUsSUFBSSxDQUFDLENBQUM7UUFDWCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7O2dCQWhKRixVQUFVOzs7O2dCQUpGLG9CQUFvQjs7SUFxSjdCLG9CQUFDO0NBQUEsQUFqSkQsSUFpSkM7U0FoSlksYUFBYTs7O0lBQ3hCLGtDQUEyQjs7SUFDM0Isb0NBQTRCOztJQUM1QixnQ0FBYTs7Ozs7SUFDYixpQ0FBc0I7Ozs7O0lBRVYscUNBQTBDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7djQgYXMgdXVpZH0gZnJvbSAndXVpZCc7XG5pbXBvcnQge1BheXBhbE1vZGFsQ29tcG9uZW50fSBmcm9tICcuL3BheXBhbC5jb21wb25lbnQnO1xuaW1wb3J0IHsgTmd4U21hcnRNb2RhbFNlcnZpY2UgfSBmcm9tICduZ3gtc21hcnQtbW9kYWwnO1xuXG5pbXBvcnQgJ3VybC1zZWFyY2gtcGFyYW1zLXBvbHlmaWxsJztcbmRlY2xhcmUgdmFyIHdpbmRvdzogYW55O1xuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFBheVBhbFNlcnZpY2Uge1xuICBhZGRTY3JpcHQ6IGJvb2xlYW4gPSBmYWxzZTtcbiAgc2NyaXB0UmVhZHk6IGJvb2xlYW4gPSB0cnVlO1xuICBoYW5kbGVyOiBhbnk7XG4gIHByaXZhdGUgbW9kYWxSZWY6IGFueTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIG1vZGFsU2VydmljZTogTmd4U21hcnRNb2RhbFNlcnZpY2UpIHtcbiAgICB0aGlzLl9faW5pdCgpO1xuICB9XG4gIF9faW5pdCgpOiB2b2lkIHtcbiAgICBpZiAoIXRoaXMuYWRkU2NyaXB0KSB7XG4gICAgICB0aGlzLmluamVjdFNjcmlwdCgpLnRoZW4oKCkgPT4ge1xuICAgICAgICB0aGlzLnNjcmlwdFJlYWR5ID0gZmFsc2U7XG4gICAgICAgIGNvbnNvbGUubG9nKCdQYXlQYWwgY2hlY2tvdXQuanMgc2NyaXB0IHJlYWR5Jyk7XG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuICBjb25maWcocGFyYW1zLCByZXNvbHZlLCByZWplY3QpIHtcbiAgICByZXR1cm4ge1xuICAgICAgXCJsb2NhbGVcIjogJ2VuX1VTJyxcbiAgICAgIFwiZW52XCI6IHBhcmFtcy5pc1Rlc3QgPyAnc2FuZGJveCcgOiAncHJvZHVjdGlvbicsXG4gICAgICBcInN0eWxlXCI6IHtcbiAgICAgICAgXCJsYXlvdXRcIjogJ3ZlcnRpY2FsJyxcbiAgICAgIH0sXG4gICAgICBcImNsaWVudFwiOiB7XG4gICAgICAgIFwic2FuZGJveFwiOiBwYXJhbXMudXNlcm5hbWUsXG4gICAgICAgIFwicHJvZHVjdGlvblwiOiBwYXJhbXMudXNlcm5hbWUsXG4gICAgICB9LFxuICAgICAgXCJjb21taXRcIjogdHJ1ZSxcbiAgICAgIFwicGF5bWVudFwiOiAoZGF0YSwgYWN0aW9ucykgPT4ge1xuICAgICAgICByZXR1cm4gYWN0aW9ucy5wYXltZW50LmNyZWF0ZSh7XG4gICAgICAgICAgXCJwYXltZW50XCI6IHtcbiAgICAgICAgICAgIFwidHJhbnNhY3Rpb25zXCI6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIFwiYW1vdW50XCI6IHtcbiAgICAgICAgICAgICAgICAgIFwidG90YWxcIjogcGFyYW1zLmFtb3VudCwgXG4gICAgICAgICAgICAgICAgICBcImN1cnJlbmN5XCI6IHBhcmFtcy5jdXJyZW5jeVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgXCJkZXNjcmlwdGlvblwiOiBwYXJhbXMuZGVzY3JpcHRpb24gfHwgJydcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgfSxcbiAgICAgICAgfSk7XG4gICAgICB9LFxuICAgICAgb25BdXRob3JpemU6IChkYXRhLCBhY3Rpb25zKSA9PiB7XG4gICAgICAgIHJldHVybiBhY3Rpb25zLnBheW1lbnRcbiAgICAgICAgICAuZXhlY3V0ZSgpXG4gICAgICAgICAgLnRoZW4ocGF5bWVudCA9PiB7XG4gICAgICAgICAgICB2YXIgdHJhbnNhY3Rpb25faWQgPSAnJztcbiAgICAgICAgICAgIGlmIChcbiAgICAgICAgICAgICAgcGF5bWVudCAmJlxuICAgICAgICAgICAgICBwYXltZW50LnRyYW5zYWN0aW9ucyAmJlxuICAgICAgICAgICAgICBwYXltZW50LnRyYW5zYWN0aW9ucy5sZW5ndGggJiZcbiAgICAgICAgICAgICAgcGF5bWVudC50cmFuc2FjdGlvbnNbMF0ucmVsYXRlZF9yZXNvdXJjZXMgJiZcbiAgICAgICAgICAgICAgcGF5bWVudC50cmFuc2FjdGlvbnNbMF0ucmVsYXRlZF9yZXNvdXJjZXMubGVuZ3RoICYmXG4gICAgICAgICAgICAgIHBheW1lbnQudHJhbnNhY3Rpb25zWzBdLnJlbGF0ZWRfcmVzb3VyY2VzWzBdLnNhbGUgJiZcbiAgICAgICAgICAgICAgcGF5bWVudC50cmFuc2FjdGlvbnNbMF0ucmVsYXRlZF9yZXNvdXJjZXNbMF0uc2FsZS5pZFxuICAgICAgICAgICAgKSB7XG4gICAgICAgICAgICAgIHRyYW5zYWN0aW9uX2lkID1cbiAgICAgICAgICAgICAgICBwYXltZW50LnRyYW5zYWN0aW9uc1swXS5yZWxhdGVkX3Jlc291cmNlc1swXS5zYWxlLmlkO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy8gY2hlY2sgaWYgdGhlIFBheVBhbCB0cmFuc2FjdGlvbiB3ZW50IHRocm91Z2hcbiAgICAgICAgICAgIGlmIChwYXltZW50LmludGVudCAhPSAnc2FsZScgJiYgcGF5bWVudC5zdGF0ZSAhPSAnYXBwcm92ZWQnKSB7XG4gICAgICAgICAgICAgIHJlamVjdChwYXltZW50KTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgIHJlc29sdmUoe1xuICAgICAgICAgICAgICAgIHRvdGFsX2Ftb3VudDogcGFyYW1zLmFtb3VudCxcbiAgICAgICAgICAgICAgICBmZWVfYW1vdW50OiAwLFxuICAgICAgICAgICAgICAgIG5ldF9hbW91bnQ6IHBhcmFtcy5hbW91bnQsXG4gICAgICAgICAgICAgICAgY3VycmVuY3k6IHBhcmFtcy5jdXJyZW5jeSxcbiAgICAgICAgICAgICAgICB0cnhuX2lkOiB0cmFuc2FjdGlvbl9pZCxcbiAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgaWYodGhpcy5tb2RhbFJlZiAmJiB0aGlzLm1vZGFsUmVmLmNsb3NlKXtcbiAgICAgICAgICAgICAgICB0aGlzLm1vZGFsUmVmLmNsb3NlKCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9KVxuICAgICAgICAgIC5jYXRjaChlID0+IHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGUpO1xuICAgICAgICAgICAgcmVqZWN0KG51bGwpO1xuICAgICAgICAgIH0pO1xuICAgICAgfSxcbiAgICB9O1xuICB9XG5cbiAgaW5qZWN0U2NyaXB0KCkge1xuICAgIHRoaXMuYWRkU2NyaXB0ID0gdHJ1ZTtcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgbGV0IHNjcmlwdHRhZ0VsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzY3JpcHQnKTtcbiAgICAgIHNjcmlwdHRhZ0VsZW1lbnQuc3JjID0gJ2h0dHBzOi8vd3d3LnBheXBhbG9iamVjdHMuY29tL2FwaS9jaGVja291dC5qcyc7XG4gICAgICBzY3JpcHR0YWdFbGVtZW50Lm9ubG9hZCA9IHJlc29sdmU7XG4gICAgICBzY3JpcHR0YWdFbGVtZW50Lm9uZXJyb3IgPSByZWplY3Q7XG4gICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKHNjcmlwdHRhZ0VsZW1lbnQpO1xuICAgIH0pO1xuICB9XG5cbiAgZmFrZUNoYXJnZShwYXJhbXM6IGFueSkge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBsZXQgdG90YWxfYW1vdW50OiBhbnkgPSBwYXJzZUludChwYXJhbXNbJ2Ftb3VudCddKTtcbiAgICAgIGxldCBmZWVfYW1vdW50OiBhbnkgPSAoKDEwMCAqIDIuOSkgLyB0b3RhbF9hbW91bnQgKyAwLjMpLnRvRml4ZWQoMik7XG4gICAgICBsZXQgbmV0X2Ftb3VudDogYW55ID0gKHRvdGFsX2Ftb3VudCAtIGZlZV9hbW91bnQpLnRvRml4ZWQoMik7XG4gICAgICByZXNvbHZlKHtcbiAgICAgICAgdHJ4bl9pZDogJ2NoXycgKyB1dWlkKCksXG4gICAgICAgIHRvdGFsX2Ftb3VudDogdG90YWxfYW1vdW50LFxuICAgICAgICBmZWVfYW1vdW50OiBmZWVfYW1vdW50LFxuICAgICAgICBuZXRfYW1vdW50OiBuZXRfYW1vdW50LFxuICAgICAgICBjdXJyZW5jeTogJ3VzZCcsXG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIGNoYXJnZShwYXJhbXM6IGFueSkge1xuICAgIGxldCBzZWxmOiBhbnkgPSB0aGlzO1xuICAgIHBhcmFtcy5hbW91bnQgPSBwYXJhbXMuYW1vdW50IHx8IDA7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIGlmICghdGhpcy5hZGRTY3JpcHQpIHtcbiAgICAgICAgY29uc29sZS5lcnJvcignUGF5UGFsIGNoZWNrb3V0LmpzIHNjcmlwdCBpcyBub3QgcmVhZHknKTtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuXG4gICAgICBjb25zdCBtb2RhbE9wdGlvbnM6IGFueSA9IHtcbiAgICAgICAgZXNjYXBhYmxlOiBmYWxzZSxcbiAgICAgICAgY3VzdG9tQ2xhc3M6ICdwYXlwYWxNb2RhbENsYXNzJ1xuICAgICAgfTtcblxuICAgICAgdGhpcy5tb2RhbFJlZiA9IHRoaXMubW9kYWxTZXJ2aWNlLmNyZWF0ZSgnUGF5cGFsTW9kYWwnLCBQYXlwYWxNb2RhbENvbXBvbmVudCwgbW9kYWxPcHRpb25zKS5vcGVuKCk7XG4gICAgICB0aGlzLm1vZGFsUmVmLm9uT3BlbkZpbmlzaGVkLnN1YnNjcmliZSgocmVzdWx0KSA9PiB7XG4gICAgICAgIHRoaXMubW9kYWxTZXJ2aWNlLnNldE1vZGFsRGF0YSh7IGlzVGVzdDogcGFyYW1zLmlzVGVzdCB9LCAnUGF5cGFsTW9kYWwnKTtcbiAgICAgIH0pO1xuICAgICAgXG4gICAgICB0aGlzLm1vZGFsUmVmLm9uQ2xvc2Uuc3Vic2NyaWJlKChyZXN1bHQpID0+IHtcbiAgICAgICAgY29uc29sZS5sb2coJ2Nsb3NlZCcsIHJlc3VsdClcbiAgICAgIH0pO1xuICAgICAgXG4gICAgICB3aW5kb3cuc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgIHdpbmRvdy5wYXlwYWwuQnV0dG9uLnJlbmRlcihcbiAgICAgICAgICBzZWxmLmNvbmZpZyhwYXJhbXMsIHJlc29sdmUsIHJlamVjdCksXG4gICAgICAgICAgJyNwYXlwYWwtYnV0dG9ucy1jb250YWluZXInLFxuICAgICAgICApO1xuICAgICAgfSwgMTAwMCk7XG4gICAgfSk7XG4gIH1cbn0iXX0=