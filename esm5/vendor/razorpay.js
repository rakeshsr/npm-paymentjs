/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, HostListener } from '@angular/core';
import { v4 as uuid } from 'uuid';
import 'url-search-params-polyfill';
var RazorpayService = /** @class */ (function () {
    function RazorpayService() {
        this.addScript = false;
        this.scriptReady = true;
        this.__init();
    }
    /**
     * @return {?}
     */
    RazorpayService.prototype.__init = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (!this.addScript) {
            this.injectScript().then((/**
             * @return {?}
             */
            function () {
                _this.scriptReady = false;
                console.log('razorpay.js script ready', window.Razorpay);
            }));
        }
    };
    /**
     * @return {?}
     */
    RazorpayService.prototype.injectScript = /**
     * @return {?}
     */
    function () {
        this.addScript = true;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            /** @type {?} */
            var scripttagElement = document.createElement('script');
            scripttagElement.src = 'https://checkout.razorpay.com/v1/checkout.js';
            scripttagElement.onload = resolve;
            scripttagElement.onerror = reject;
            document.body.appendChild(scripttagElement);
        }));
    };
    /**
     * @param {?} params
     * @return {?}
     */
    RazorpayService.prototype.fakeCharge = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            /** @type {?} */
            var total_amount = parseInt(params["amount"]);
            /** @type {?} */
            var fee_amount = (((100 * 2.9) / total_amount) + 0.30).toFixed(2);
            /** @type {?} */
            var net_amount = (total_amount - fee_amount).toFixed(2);
            resolve({
                "trxn_id": "ch_" + uuid(),
                "total_amount": total_amount,
                "fee_amount": fee_amount,
                "net_amount": net_amount,
                "currency": "usd"
            });
        }));
    };
    /**
     * @param {?} params
     * @return {?}
     */
    RazorpayService.prototype.charge = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        /** @type {?} */
        var self = this;
        /** @type {?} */
        var amount = (params.amount || 0) * 100;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            /** @type {?} */
            var options = {
                "key": params.username,
                "amount": amount,
                "currency": params.currency || "INR",
                "name": params.name || '',
                "description": params.description,
                "image": params.image || '',
                "prefill": {
                    "email": params.email || '',
                    "contact": params.phonenumber || ''
                },
                "modal": {
                    "ondismiss": (/**
                     * @return {?}
                     */
                    function () {
                        reject('Razorpay modal closed');
                    })
                },
                handler: (/**
                 * @param {?} token
                 * @return {?}
                 */
                function (token) {
                    resolve({
                        "total_amount": params.amount,
                        "fee_amount": 0,
                        "net_amount": params.amount,
                        "currency": params.currency,
                        "trxn_id": token.razorpay_payment_id,
                        "raw_response": token
                    });
                    return false;
                })
            }
            // optional
            ;
            // optional
            if (params.order_id) {
                options.order_id = params.order_id;
            }
            self.handler = new window.Razorpay(options);
            self.handler.open();
        }));
    };
    /**
     * @return {?}
     */
    RazorpayService.prototype.onPopstate = /**
     * @return {?}
     */
    function () {
        this.handler.close();
    };
    RazorpayService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    RazorpayService.ctorParameters = function () { return []; };
    RazorpayService.propDecorators = {
        onPopstate: [{ type: HostListener, args: ['window:popstate',] }]
    };
    return RazorpayService;
}());
export { RazorpayService };
if (false) {
    /** @type {?} */
    RazorpayService.prototype.addScript;
    /** @type {?} */
    RazorpayService.prototype.scriptReady;
    /** @type {?} */
    RazorpayService.prototype.handler;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmF6b3JwYXkuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9wYXltZW50anMvIiwic291cmNlcyI6WyJ2ZW5kb3IvcmF6b3JwYXkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pELE9BQU8sRUFBRSxFQUFFLElBQUksSUFBSSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2xDLE9BQU8sNEJBQTRCLENBQUE7QUFHbkM7SUFNRTtRQUpBLGNBQVMsR0FBWSxLQUFLLENBQUM7UUFDM0IsZ0JBQVcsR0FBWSxJQUFJLENBQUM7UUFHWixJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7SUFBQyxDQUFDOzs7O0lBRWhDLGdDQUFNOzs7SUFBTjtRQUFBLGlCQU9DO1FBTkMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDbkIsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLElBQUk7OztZQUFDO2dCQUN2QixLQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztnQkFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQywwQkFBMEIsRUFBRSxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDM0QsQ0FBQyxFQUFDLENBQUE7U0FDSDtJQUNILENBQUM7Ozs7SUFFRCxzQ0FBWTs7O0lBQVo7UUFDRSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN0QixPQUFPLElBQUksT0FBTzs7Ozs7UUFBQyxVQUFDLE9BQU8sRUFBRSxNQUFNOztnQkFDN0IsZ0JBQWdCLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUM7WUFDdkQsZ0JBQWdCLENBQUMsR0FBRyxHQUFHLDhDQUE4QyxDQUFDO1lBQ3RFLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUM7WUFDbEMsZ0JBQWdCLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztZQUNsQyxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQzlDLENBQUMsRUFBQyxDQUFBO0lBQ0osQ0FBQzs7Ozs7SUFFRCxvQ0FBVTs7OztJQUFWLFVBQVcsTUFBVztRQUNwQixPQUFPLElBQUksT0FBTzs7Ozs7UUFBQyxVQUFDLE9BQU8sRUFBRSxNQUFNOztnQkFDN0IsWUFBWSxHQUFPLFFBQVEsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7O2dCQUM3QyxVQUFVLEdBQU8sQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxHQUFHLFlBQVksQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7O2dCQUNqRSxVQUFVLEdBQU8sQ0FBQyxZQUFZLEdBQUcsVUFBVSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUUzRCxPQUFPLENBQUM7Z0JBQ04sU0FBUyxFQUFFLEtBQUssR0FBRyxJQUFJLEVBQUU7Z0JBQ3pCLGNBQWMsRUFBRSxZQUFZO2dCQUM1QixZQUFZLEVBQUUsVUFBVTtnQkFDeEIsWUFBWSxFQUFFLFVBQVU7Z0JBQ3hCLFVBQVUsRUFBRSxLQUFLO2FBQ2xCLENBQUMsQ0FBQztRQUNMLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxnQ0FBTTs7OztJQUFOLFVBQU8sTUFBVzs7WUFDWixJQUFJLEdBQVEsSUFBSTs7WUFDZCxNQUFNLEdBQUcsQ0FBQyxNQUFNLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxHQUFHLEdBQUc7UUFFekMsT0FBTyxJQUFJLE9BQU87Ozs7O1FBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTs7Z0JBQzdCLE9BQU8sR0FBUTtnQkFDakIsS0FBSyxFQUFFLE1BQU0sQ0FBQyxRQUFRO2dCQUN0QixRQUFRLEVBQUUsTUFBTTtnQkFDaEIsVUFBVSxFQUFFLE1BQU0sQ0FBQyxRQUFRLElBQUksS0FBSztnQkFDcEMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxJQUFJLElBQUksRUFBRTtnQkFDekIsYUFBYSxFQUFFLE1BQU0sQ0FBQyxXQUFXO2dCQUNqQyxPQUFPLEVBQUUsTUFBTSxDQUFDLEtBQUssSUFBSSxFQUFFO2dCQUMzQixTQUFTLEVBQUU7b0JBQ1QsT0FBTyxFQUFFLE1BQU0sQ0FBQyxLQUFLLElBQUksRUFBRTtvQkFDM0IsU0FBUyxFQUFFLE1BQU0sQ0FBQyxXQUFXLElBQUksRUFBRTtpQkFDcEM7Z0JBQ0QsT0FBTyxFQUFFO29CQUNQLFdBQVc7OztvQkFBRTt3QkFDWCxNQUFNLENBQUMsdUJBQXVCLENBQUMsQ0FBQTtvQkFDakMsQ0FBQyxDQUFBO2lCQUNGO2dCQUNELE9BQU87Ozs7Z0JBQUUsVUFBUyxLQUFLO29CQUNyQixPQUFPLENBQUM7d0JBQ04sY0FBYyxFQUFFLE1BQU0sQ0FBQyxNQUFNO3dCQUM3QixZQUFZLEVBQUUsQ0FBQzt3QkFDZixZQUFZLEVBQUUsTUFBTSxDQUFDLE1BQU07d0JBQzNCLFVBQVUsRUFBRSxNQUFNLENBQUMsUUFBUTt3QkFDM0IsU0FBUyxFQUFFLEtBQUssQ0FBQyxtQkFBbUI7d0JBQ3BDLGNBQWMsRUFBRSxLQUFLO3FCQUN0QixDQUFDLENBQUM7b0JBQ0gsT0FBTyxLQUFLLENBQUM7Z0JBQ2YsQ0FBQyxDQUFBO2FBQ0Y7WUFFRCxXQUFXOztZQUFYLFdBQVc7WUFDWCxJQUFHLE1BQU0sQ0FBQyxRQUFRLEVBQUM7Z0JBQ2pCLE9BQU8sQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQzthQUNwQztZQUVELElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzVDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDdEIsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7O0lBR0Qsb0NBQVU7OztJQURWO1FBRUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQTtJQUN0QixDQUFDOztnQkEzRkYsVUFBVTs7Ozs7NkJBd0ZSLFlBQVksU0FBQyxpQkFBaUI7O0lBSWpDLHNCQUFDO0NBQUEsQUE1RkQsSUE0RkM7U0EzRlksZUFBZTs7O0lBQzFCLG9DQUEyQjs7SUFDM0Isc0NBQTRCOztJQUM1QixrQ0FBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIEhvc3RMaXN0ZW5lciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgdjQgYXMgdXVpZCB9IGZyb20gJ3V1aWQnO1xuaW1wb3J0ICd1cmwtc2VhcmNoLXBhcmFtcy1wb2x5ZmlsbCdcbmRlY2xhcmUgdmFyIHdpbmRvdzogYW55O1xu4oCLXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgUmF6b3JwYXlTZXJ2aWNlIHtcbiAgYWRkU2NyaXB0OiBib29sZWFuID0gZmFsc2U7XG4gIHNjcmlwdFJlYWR5OiBib29sZWFuID0gdHJ1ZTtcbiAgaGFuZGxlcjogYW55O1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB0aGlzLl9faW5pdCgpOyB9XG7igItcbiAgX19pbml0KCk6IHZvaWQge1xuICAgIGlmICghdGhpcy5hZGRTY3JpcHQpIHtcbiAgICAgIHRoaXMuaW5qZWN0U2NyaXB0KCkudGhlbigoKSA9PiB7XG4gICAgICAgIHRoaXMuc2NyaXB0UmVhZHkgPSBmYWxzZTtcbiAgICAgICAgY29uc29sZS5sb2coJ3Jhem9ycGF5LmpzIHNjcmlwdCByZWFkeScsIHdpbmRvdy5SYXpvcnBheSk7XG4gICAgICB9KVxuICAgIH1cbiAgfVxuXG4gIGluamVjdFNjcmlwdCgpIHtcbiAgICB0aGlzLmFkZFNjcmlwdCA9IHRydWU7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIGxldCBzY3JpcHR0YWdFbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc2NyaXB0Jyk7XG4gICAgICBzY3JpcHR0YWdFbGVtZW50LnNyYyA9ICdodHRwczovL2NoZWNrb3V0LnJhem9ycGF5LmNvbS92MS9jaGVja291dC5qcyc7XG4gICAgICBzY3JpcHR0YWdFbGVtZW50Lm9ubG9hZCA9IHJlc29sdmU7XG4gICAgICBzY3JpcHR0YWdFbGVtZW50Lm9uZXJyb3IgPSByZWplY3Q7XG4gICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKHNjcmlwdHRhZ0VsZW1lbnQpO1xuICAgIH0pXG4gIH1cblxuICBmYWtlQ2hhcmdlKHBhcmFtczogYW55KSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIGxldCB0b3RhbF9hbW91bnQ6YW55ID0gcGFyc2VJbnQocGFyYW1zW1wiYW1vdW50XCJdKTtcbiAgICAgIGxldCBmZWVfYW1vdW50OmFueSA9ICgoKDEwMCAqIDIuOSkgLyB0b3RhbF9hbW91bnQpICsgMC4zMCkudG9GaXhlZCgyKTtcbiAgICAgIGxldCBuZXRfYW1vdW50OmFueSA9ICh0b3RhbF9hbW91bnQgLSBmZWVfYW1vdW50KS50b0ZpeGVkKDIpO1xu4oCLXG4gICAgICByZXNvbHZlKHtcbiAgICAgICAgXCJ0cnhuX2lkXCI6IFwiY2hfXCIgKyB1dWlkKCksXG4gICAgICAgIFwidG90YWxfYW1vdW50XCI6IHRvdGFsX2Ftb3VudCxcbiAgICAgICAgXCJmZWVfYW1vdW50XCI6IGZlZV9hbW91bnQsXG4gICAgICAgIFwibmV0X2Ftb3VudFwiOiBuZXRfYW1vdW50LFxuICAgICAgICBcImN1cnJlbmN5XCI6IFwidXNkXCJcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG7igItcbiAgY2hhcmdlKHBhcmFtczogYW55KSB7XG4gICAgbGV0IHNlbGY6IGFueSA9IHRoaXM7XG4gICAgY29uc3QgYW1vdW50ID0gKHBhcmFtcy5hbW91bnQgfHwgMCkgKiAxMDBcblxuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBsZXQgb3B0aW9uczogYW55ID0ge1xuICAgICAgICBcImtleVwiOiBwYXJhbXMudXNlcm5hbWUsXG4gICAgICAgIFwiYW1vdW50XCI6IGFtb3VudCxcbiAgICAgICAgXCJjdXJyZW5jeVwiOiBwYXJhbXMuY3VycmVuY3kgfHwgXCJJTlJcIixcbiAgICAgICAgXCJuYW1lXCI6IHBhcmFtcy5uYW1lIHx8ICcnLFxuICAgICAgICBcImRlc2NyaXB0aW9uXCI6IHBhcmFtcy5kZXNjcmlwdGlvbixcbiAgICAgICAgXCJpbWFnZVwiOiBwYXJhbXMuaW1hZ2UgfHwgJycsXG4gICAgICAgIFwicHJlZmlsbFwiOiB7XG4gICAgICAgICAgXCJlbWFpbFwiOiBwYXJhbXMuZW1haWwgfHwgJycsXG4gICAgICAgICAgXCJjb250YWN0XCI6IHBhcmFtcy5waG9uZW51bWJlciB8fCAnJ1xuICAgICAgICB9LFxuICAgICAgICBcIm1vZGFsXCI6IHtcbiAgICAgICAgICBcIm9uZGlzbWlzc1wiOiBmdW5jdGlvbigpe1xuICAgICAgICAgICAgcmVqZWN0KCdSYXpvcnBheSBtb2RhbCBjbG9zZWQnKVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgaGFuZGxlcjogZnVuY3Rpb24odG9rZW4pIHtcbiAgICAgICAgICByZXNvbHZlKHtcbiAgICAgICAgICAgIFwidG90YWxfYW1vdW50XCI6IHBhcmFtcy5hbW91bnQsXG4gICAgICAgICAgICBcImZlZV9hbW91bnRcIjogMCxcbiAgICAgICAgICAgIFwibmV0X2Ftb3VudFwiOiBwYXJhbXMuYW1vdW50LFxuICAgICAgICAgICAgXCJjdXJyZW5jeVwiOiBwYXJhbXMuY3VycmVuY3ksXG4gICAgICAgICAgICBcInRyeG5faWRcIjogdG9rZW4ucmF6b3JwYXlfcGF5bWVudF9pZCxcbiAgICAgICAgICAgIFwicmF3X3Jlc3BvbnNlXCI6IHRva2VuICAgIFxuICAgICAgICAgIH0pO1xuICAgICAgICAgIHJldHVybiBmYWxzZTsgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgLy8gb3B0aW9uYWxcbiAgICAgIGlmKHBhcmFtcy5vcmRlcl9pZCl7XG4gICAgICAgIG9wdGlvbnMub3JkZXJfaWQgPSBwYXJhbXMub3JkZXJfaWQ7XG4gICAgICB9XG5cbiAgICAgIHNlbGYuaGFuZGxlciA9IG5ldyB3aW5kb3cuUmF6b3JwYXkob3B0aW9ucyk7XG4gICAgICBzZWxmLmhhbmRsZXIub3BlbigpO1xuICAgIH0pO1xuICB9XG7igItcbiAgQEhvc3RMaXN0ZW5lcignd2luZG93OnBvcHN0YXRlJylcbiAgb25Qb3BzdGF0ZSgpIHtcbiAgICB0aGlzLmhhbmRsZXIuY2xvc2UoKVxuICB9XG59Il19