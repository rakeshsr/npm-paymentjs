import { __awaiter, __generator } from 'tslib';
import { Injectable, HostListener, Component, NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { v4 } from 'uuid';
import 'url-search-params-polyfill';
import { NgxSmartModalService, NgxSmartModalModule } from 'ngx-smart-modal';
import { CommonModule } from '@angular/common';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var StripeService = /** @class */ (function () {
    function StripeService(http) {
        this.http = http;
        this.addScript = false;
        this.scriptReady = true;
        this.__init();
    }
    /**
     * @return {?}
     */
    StripeService.prototype.__init = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (!this.addScript) {
            this.injectScript().then((/**
             * @return {?}
             */
            function () {
                _this.scriptReady = false;
                console.log('stripe.js script ready', window.StripeCheckout);
            }));
        }
    };
    /**
     * @return {?}
     */
    StripeService.prototype.injectScript = /**
     * @return {?}
     */
    function () {
        this.addScript = true;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            /** @type {?} */
            var scripttagElement = document.createElement('script');
            scripttagElement.src = 'https://checkout.stripe.com/checkout.js';
            scripttagElement.onload = resolve;
            scripttagElement.onerror = reject;
            document.body.appendChild(scripttagElement);
        }));
    };
    /**
     * @param {?} params
     * @return {?}
     */
    StripeService.prototype.fakeCharge = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            /** @type {?} */
            var total_amount = parseInt(params["amount"]);
            /** @type {?} */
            var fee_amount = (((100 * 2.9) / total_amount) + 0.30).toFixed(2);
            /** @type {?} */
            var net_amount = (total_amount - fee_amount).toFixed(2);
            resolve({
                "trxn_id": "ch_" + v4(),
                "total_amount": total_amount,
                "fee_amount": fee_amount,
                "net_amount": net_amount,
                "currency": "usd"
            });
        }));
    };
    /**
     * @param {?} params
     * @return {?}
     */
    StripeService.prototype.charge = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        /** @type {?} */
        var self = this;
        params.amount = (params.amount || 0) * 100;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            self.handler = window.StripeCheckout.configure({
                key: params.username,
                locale: 'auto',
                name: params.name || '',
                image: params.image || '',
                billingAddress: false,
                zipCode: false,
                allowRememberMe: false,
                description: params.description || '',
                closed: (/**
                 * @param {?} e
                 * @return {?}
                 */
                function (e) {
                    if (!self.handler.isTokenGenerate) {
                        reject({
                            "code": "stripe_transaction_cancelled"
                        });
                    }
                }),
                token: (/**
                 * @param {?} token
                 * @return {?}
                 */
                function (token) {
                    resolve({
                        "total_amount": (params.amount / 100),
                        "fee_amount": 0,
                        "net_amount": (params.amount / 100),
                        "currency": params.currency,
                        "trxn_id": token.id
                    });
                    return false;
                })
            });
            self.handler.open(params);
        }));
    };
    /**
     * @return {?}
     */
    StripeService.prototype.onPopstate = /**
     * @return {?}
     */
    function () {
        this.handler.close();
    };
    StripeService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    StripeService.ctorParameters = function () { return [
        { type: HttpClient }
    ]; };
    StripeService.propDecorators = {
        onPopstate: [{ type: HostListener, args: ['window:popstate',] }]
    };
    return StripeService;
}());
if (false) {
    /** @type {?} */
    StripeService.prototype.addScript;
    /** @type {?} */
    StripeService.prototype.scriptReady;
    /** @type {?} */
    StripeService.prototype.handler;
    /**
     * @type {?}
     * @private
     */
    StripeService.prototype.http;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var RazorpayService = /** @class */ (function () {
    function RazorpayService() {
        this.addScript = false;
        this.scriptReady = true;
        this.__init();
    }
    /**
     * @return {?}
     */
    RazorpayService.prototype.__init = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (!this.addScript) {
            this.injectScript().then((/**
             * @return {?}
             */
            function () {
                _this.scriptReady = false;
                console.log('razorpay.js script ready', window.Razorpay);
            }));
        }
    };
    /**
     * @return {?}
     */
    RazorpayService.prototype.injectScript = /**
     * @return {?}
     */
    function () {
        this.addScript = true;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            /** @type {?} */
            var scripttagElement = document.createElement('script');
            scripttagElement.src = 'https://checkout.razorpay.com/v1/checkout.js';
            scripttagElement.onload = resolve;
            scripttagElement.onerror = reject;
            document.body.appendChild(scripttagElement);
        }));
    };
    /**
     * @param {?} params
     * @return {?}
     */
    RazorpayService.prototype.fakeCharge = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            /** @type {?} */
            var total_amount = parseInt(params["amount"]);
            /** @type {?} */
            var fee_amount = (((100 * 2.9) / total_amount) + 0.30).toFixed(2);
            /** @type {?} */
            var net_amount = (total_amount - fee_amount).toFixed(2);
            resolve({
                "trxn_id": "ch_" + v4(),
                "total_amount": total_amount,
                "fee_amount": fee_amount,
                "net_amount": net_amount,
                "currency": "usd"
            });
        }));
    };
    /**
     * @param {?} params
     * @return {?}
     */
    RazorpayService.prototype.charge = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        /** @type {?} */
        var self = this;
        /** @type {?} */
        var amount = (params.amount || 0) * 100;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            /** @type {?} */
            var options = {
                "key": params.username,
                "amount": amount,
                "currency": params.currency || "INR",
                "name": params.name || '',
                "description": params.description,
                "image": params.image || '',
                "prefill": {
                    "email": params.email || '',
                    "contact": params.phonenumber || ''
                },
                "modal": {
                    "ondismiss": (/**
                     * @return {?}
                     */
                    function () {
                        reject('Razorpay modal closed');
                    })
                },
                handler: (/**
                 * @param {?} token
                 * @return {?}
                 */
                function (token) {
                    resolve({
                        "total_amount": params.amount,
                        "fee_amount": 0,
                        "net_amount": params.amount,
                        "currency": params.currency,
                        "trxn_id": token.razorpay_payment_id,
                        "raw_response": token
                    });
                    return false;
                })
            }
            // optional
            ;
            // optional
            if (params.order_id) {
                options.order_id = params.order_id;
            }
            self.handler = new window.Razorpay(options);
            self.handler.open();
        }));
    };
    /**
     * @return {?}
     */
    RazorpayService.prototype.onPopstate = /**
     * @return {?}
     */
    function () {
        this.handler.close();
    };
    RazorpayService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    RazorpayService.ctorParameters = function () { return []; };
    RazorpayService.propDecorators = {
        onPopstate: [{ type: HostListener, args: ['window:popstate',] }]
    };
    return RazorpayService;
}());
if (false) {
    /** @type {?} */
    RazorpayService.prototype.addScript;
    /** @type {?} */
    RazorpayService.prototype.scriptReady;
    /** @type {?} */
    RazorpayService.prototype.handler;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var PaypalModalComponent = /** @class */ (function () {
    function PaypalModalComponent(modalService) {
        this.modalService = modalService;
        this.data = {};
    }
    /**
     * @return {?}
     */
    PaypalModalComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var modalRef = this.modalService.getTopOpenedModal();
        modalRef.onDataAdded.subscribe((/**
         * @param {?} data
         * @return {?}
         */
        function (data) {
            _this.data = data;
        }));
    };
    PaypalModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'paypal-modal-component',
                    template: "<div *ngIf=\"data.isTest\" style=\"position: fixed;left: 0;top: -23px;padding: 2px 10px;background-color: #3F51B5;color: white;font-size: 0.9rem;border-top-left-radius: 5px;border-top-right-radius: 5px;\">PayPal: Test mode</div>\n<h4 class=\"modal-title\" id=\"modal-basic-title\">PayPal Payment</h4>\n<div class=\"modal-body\" style=\"overflow: auto;\">\n  <div id=\"paypal-buttons-container\" style=\"max-height: 400px;\"></div>\n</div>\n",
                    styles: [".paypalModalClass { width: 360px; }"]
                }] }
    ];
    /** @nocollapse */
    PaypalModalComponent.ctorParameters = function () { return [
        { type: NgxSmartModalService }
    ]; };
    return PaypalModalComponent;
}());
if (false) {
    /** @type {?} */
    PaypalModalComponent.prototype.data;
    /** @type {?} */
    PaypalModalComponent.prototype.modalService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var PayPalService = /** @class */ (function () {
    function PayPalService(modalService) {
        this.modalService = modalService;
        this.addScript = false;
        this.scriptReady = true;
        this.__init();
    }
    /**
     * @return {?}
     */
    PayPalService.prototype.__init = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (!this.addScript) {
            this.injectScript().then((/**
             * @return {?}
             */
            function () {
                _this.scriptReady = false;
                console.log('PayPal checkout.js script ready');
            }));
        }
    };
    /**
     * @param {?} params
     * @param {?} resolve
     * @param {?} reject
     * @return {?}
     */
    PayPalService.prototype.config = /**
     * @param {?} params
     * @param {?} resolve
     * @param {?} reject
     * @return {?}
     */
    function (params, resolve, reject) {
        var _this = this;
        return {
            "locale": 'en_US',
            "env": params.isTest ? 'sandbox' : 'production',
            "style": {
                "layout": 'vertical',
            },
            "client": {
                "sandbox": params.username,
                "production": params.username,
            },
            "commit": true,
            "payment": (/**
             * @param {?} data
             * @param {?} actions
             * @return {?}
             */
            function (data, actions) {
                return actions.payment.create({
                    "payment": {
                        "transactions": [
                            {
                                "amount": {
                                    "total": params.amount,
                                    "currency": params.currency
                                },
                                "description": params.description || ''
                            },
                        ],
                    },
                });
            }),
            onAuthorize: (/**
             * @param {?} data
             * @param {?} actions
             * @return {?}
             */
            function (data, actions) {
                return actions.payment
                    .execute()
                    .then((/**
                 * @param {?} payment
                 * @return {?}
                 */
                function (payment) {
                    /** @type {?} */
                    var transaction_id = '';
                    if (payment &&
                        payment.transactions &&
                        payment.transactions.length &&
                        payment.transactions[0].related_resources &&
                        payment.transactions[0].related_resources.length &&
                        payment.transactions[0].related_resources[0].sale &&
                        payment.transactions[0].related_resources[0].sale.id) {
                        transaction_id =
                            payment.transactions[0].related_resources[0].sale.id;
                    }
                    // check if the PayPal transaction went through
                    if (payment.intent != 'sale' && payment.state != 'approved') {
                        reject(payment);
                    }
                    else {
                        resolve({
                            total_amount: params.amount,
                            fee_amount: 0,
                            net_amount: params.amount,
                            currency: params.currency,
                            trxn_id: transaction_id,
                        });
                        if (_this.modalRef && _this.modalRef.close) {
                            _this.modalRef.close();
                        }
                    }
                }))
                    .catch((/**
                 * @param {?} e
                 * @return {?}
                 */
                function (e) {
                    console.log(e);
                    reject(null);
                }));
            }),
        };
    };
    /**
     * @return {?}
     */
    PayPalService.prototype.injectScript = /**
     * @return {?}
     */
    function () {
        this.addScript = true;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            /** @type {?} */
            var scripttagElement = document.createElement('script');
            scripttagElement.src = 'https://www.paypalobjects.com/api/checkout.js';
            scripttagElement.onload = resolve;
            scripttagElement.onerror = reject;
            document.body.appendChild(scripttagElement);
        }));
    };
    /**
     * @param {?} params
     * @return {?}
     */
    PayPalService.prototype.fakeCharge = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            /** @type {?} */
            var total_amount = parseInt(params['amount']);
            /** @type {?} */
            var fee_amount = ((100 * 2.9) / total_amount + 0.3).toFixed(2);
            /** @type {?} */
            var net_amount = (total_amount - fee_amount).toFixed(2);
            resolve({
                trxn_id: 'ch_' + v4(),
                total_amount: total_amount,
                fee_amount: fee_amount,
                net_amount: net_amount,
                currency: 'usd',
            });
        }));
    };
    /**
     * @param {?} params
     * @return {?}
     */
    PayPalService.prototype.charge = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        var _this = this;
        /** @type {?} */
        var self = this;
        params.amount = params.amount || 0;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            if (!_this.addScript) {
                console.error('PayPal checkout.js script is not ready');
                return false;
            }
            /** @type {?} */
            var modalOptions = {
                escapable: false,
                customClass: 'paypalModalClass'
            };
            _this.modalRef = _this.modalService.create('PaypalModal', PaypalModalComponent, modalOptions).open();
            _this.modalRef.onOpenFinished.subscribe((/**
             * @param {?} result
             * @return {?}
             */
            function (result) {
                _this.modalService.setModalData({ isTest: params.isTest }, 'PaypalModal');
            }));
            _this.modalRef.onClose.subscribe((/**
             * @param {?} result
             * @return {?}
             */
            function (result) {
                console.log('closed', result);
            }));
            window.setTimeout((/**
             * @return {?}
             */
            function () {
                window.paypal.Button.render(self.config(params, resolve, reject), '#paypal-buttons-container');
            }), 1000);
        }));
    };
    PayPalService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    PayPalService.ctorParameters = function () { return [
        { type: NgxSmartModalService }
    ]; };
    return PayPalService;
}());
if (false) {
    /** @type {?} */
    PayPalService.prototype.addScript;
    /** @type {?} */
    PayPalService.prototype.scriptReady;
    /** @type {?} */
    PayPalService.prototype.handler;
    /**
     * @type {?}
     * @private
     */
    PayPalService.prototype.modalRef;
    /**
     * @type {?}
     * @private
     */
    PayPalService.prototype.modalService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var PaymentjsService = /** @class */ (function () {
    function PaymentjsService(stripe, razorpay, paypal) {
        this.stripe = stripe;
        this.razorpay = razorpay;
        this.paypal = paypal;
    }
    /**
     * @param {?} data
     * @return {?}
     */
    PaymentjsService.prototype.checkout = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        var _this = this;
        /** @type {?} */
        var result;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var payment_type, _a, e_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        payment_type = data.paymentInstrument.toLowerCase();
                        _b.label = 1;
                    case 1:
                        _b.trys.push([1, 10, , 11]);
                        _a = payment_type;
                        switch (_a) {
                            case 'payment_stripe': return [3 /*break*/, 2];
                            case 'payment_razorpay': return [3 /*break*/, 4];
                            case 'payment_paypal': return [3 /*break*/, 6];
                        }
                        return [3 /*break*/, 8];
                    case 2: return [4 /*yield*/, this.stripe.charge(data.params)];
                    case 3:
                        result = _b.sent();
                        return [3 /*break*/, 9];
                    case 4: return [4 /*yield*/, this.razorpay.charge(data.params)];
                    case 5:
                        result = _b.sent();
                        return [3 /*break*/, 9];
                    case 6: return [4 /*yield*/, this.paypal.charge(data.params)];
                    case 7:
                        result = _b.sent();
                        return [3 /*break*/, 9];
                    case 8:
                        reject("Unsupported Payment Instrument " + data.paymentInstrument);
                        return [2 /*return*/, false];
                    case 9:
                        result["isTest"] = data.params.isTest;
                        result["paymentInstrument"] = data.paymentInstrument;
                        resolve(result);
                        return [3 /*break*/, 11];
                    case 10:
                        e_1 = _b.sent();
                        console.log(e_1);
                        reject('Transaction cancelled!');
                        return [3 /*break*/, 11];
                    case 11: return [2 /*return*/];
                }
            });
        }); }));
    };
    PaymentjsService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    PaymentjsService.ctorParameters = function () { return [
        { type: StripeService },
        { type: RazorpayService },
        { type: PayPalService }
    ]; };
    return PaymentjsService;
}());
if (false) {
    /**
     * @type {?}
     * @private
     */
    PaymentjsService.prototype.stripe;
    /**
     * @type {?}
     * @private
     */
    PaymentjsService.prototype.razorpay;
    /**
     * @type {?}
     * @private
     */
    PaymentjsService.prototype.paypal;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var PaymentjsComponent = /** @class */ (function () {
    function PaymentjsComponent() {
    }
    /**
     * @return {?}
     */
    PaymentjsComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    PaymentjsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lib-paymentjs',
                    template: "\n    <p>\n      paymentjs works!\n    </p>\n  "
                }] }
    ];
    /** @nocollapse */
    PaymentjsComponent.ctorParameters = function () { return []; };
    return PaymentjsComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var PaymentjsModule = /** @class */ (function () {
    function PaymentjsModule() {
    }
    PaymentjsModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [PaymentjsComponent, PaypalModalComponent],
                    imports: [CommonModule, NgxSmartModalModule.forRoot()],
                    entryComponents: [PaypalModalComponent],
                    providers: [PaymentjsService, StripeService, RazorpayService, PayPalService],
                    exports: [PaymentjsComponent, PaypalModalComponent]
                },] }
    ];
    return PaymentjsModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { PaymentjsComponent, PaymentjsModule, PaymentjsService, StripeService as ɵa, RazorpayService as ɵb, PayPalService as ɵc, PaypalModalComponent as ɵd };
//# sourceMappingURL=paymentjs.js.map
