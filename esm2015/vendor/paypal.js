/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { v4 as uuid } from 'uuid';
import { PaypalModalComponent } from './paypal.component';
import { NgxSmartModalService } from 'ngx-smart-modal';
import 'url-search-params-polyfill';
export class PayPalService {
    /**
     * @param {?} modalService
     */
    constructor(modalService) {
        this.modalService = modalService;
        this.addScript = false;
        this.scriptReady = true;
        this.__init();
    }
    /**
     * @return {?}
     */
    __init() {
        if (!this.addScript) {
            this.injectScript().then((/**
             * @return {?}
             */
            () => {
                this.scriptReady = false;
                console.log('PayPal checkout.js script ready');
            }));
        }
    }
    /**
     * @param {?} params
     * @param {?} resolve
     * @param {?} reject
     * @return {?}
     */
    config(params, resolve, reject) {
        return {
            "locale": 'en_US',
            "env": params.isTest ? 'sandbox' : 'production',
            "style": {
                "layout": 'vertical',
            },
            "client": {
                "sandbox": params.username,
                "production": params.username,
            },
            "commit": true,
            "payment": (/**
             * @param {?} data
             * @param {?} actions
             * @return {?}
             */
            (data, actions) => {
                return actions.payment.create({
                    "payment": {
                        "transactions": [
                            {
                                "amount": {
                                    "total": params.amount,
                                    "currency": params.currency
                                },
                                "description": params.description || ''
                            },
                        ],
                    },
                });
            }),
            onAuthorize: (/**
             * @param {?} data
             * @param {?} actions
             * @return {?}
             */
            (data, actions) => {
                return actions.payment
                    .execute()
                    .then((/**
                 * @param {?} payment
                 * @return {?}
                 */
                payment => {
                    /** @type {?} */
                    var transaction_id = '';
                    if (payment &&
                        payment.transactions &&
                        payment.transactions.length &&
                        payment.transactions[0].related_resources &&
                        payment.transactions[0].related_resources.length &&
                        payment.transactions[0].related_resources[0].sale &&
                        payment.transactions[0].related_resources[0].sale.id) {
                        transaction_id =
                            payment.transactions[0].related_resources[0].sale.id;
                    }
                    // check if the PayPal transaction went through
                    if (payment.intent != 'sale' && payment.state != 'approved') {
                        reject(payment);
                    }
                    else {
                        resolve({
                            total_amount: params.amount,
                            fee_amount: 0,
                            net_amount: params.amount,
                            currency: params.currency,
                            trxn_id: transaction_id,
                        });
                        if (this.modalRef && this.modalRef.close) {
                            this.modalRef.close();
                        }
                    }
                }))
                    .catch((/**
                 * @param {?} e
                 * @return {?}
                 */
                e => {
                    console.log(e);
                    reject(null);
                }));
            }),
        };
    }
    /**
     * @return {?}
     */
    injectScript() {
        this.addScript = true;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            /** @type {?} */
            let scripttagElement = document.createElement('script');
            scripttagElement.src = 'https://www.paypalobjects.com/api/checkout.js';
            scripttagElement.onload = resolve;
            scripttagElement.onerror = reject;
            document.body.appendChild(scripttagElement);
        }));
    }
    /**
     * @param {?} params
     * @return {?}
     */
    fakeCharge(params) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            /** @type {?} */
            let total_amount = parseInt(params['amount']);
            /** @type {?} */
            let fee_amount = ((100 * 2.9) / total_amount + 0.3).toFixed(2);
            /** @type {?} */
            let net_amount = (total_amount - fee_amount).toFixed(2);
            resolve({
                trxn_id: 'ch_' + uuid(),
                total_amount: total_amount,
                fee_amount: fee_amount,
                net_amount: net_amount,
                currency: 'usd',
            });
        }));
    }
    /**
     * @param {?} params
     * @return {?}
     */
    charge(params) {
        /** @type {?} */
        let self = this;
        params.amount = params.amount || 0;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            if (!this.addScript) {
                console.error('PayPal checkout.js script is not ready');
                return false;
            }
            /** @type {?} */
            const modalOptions = {
                escapable: false,
                customClass: 'paypalModalClass'
            };
            this.modalRef = this.modalService.create('PaypalModal', PaypalModalComponent, modalOptions).open();
            this.modalRef.onOpenFinished.subscribe((/**
             * @param {?} result
             * @return {?}
             */
            (result) => {
                this.modalService.setModalData({ isTest: params.isTest }, 'PaypalModal');
            }));
            this.modalRef.onClose.subscribe((/**
             * @param {?} result
             * @return {?}
             */
            (result) => {
                console.log('closed', result);
            }));
            window.setTimeout((/**
             * @return {?}
             */
            () => {
                window.paypal.Button.render(self.config(params, resolve, reject), '#paypal-buttons-container');
            }), 1000);
        }));
    }
}
PayPalService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
PayPalService.ctorParameters = () => [
    { type: NgxSmartModalService }
];
if (false) {
    /** @type {?} */
    PayPalService.prototype.addScript;
    /** @type {?} */
    PayPalService.prototype.scriptReady;
    /** @type {?} */
    PayPalService.prototype.handler;
    /**
     * @type {?}
     * @private
     */
    PayPalService.prototype.modalRef;
    /**
     * @type {?}
     * @private
     */
    PayPalService.prototype.modalService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGF5cGFsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vcGF5bWVudGpzLyIsInNvdXJjZXMiOlsidmVuZG9yL3BheXBhbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUMsRUFBRSxJQUFJLElBQUksRUFBQyxNQUFNLE1BQU0sQ0FBQztBQUNoQyxPQUFPLEVBQUMsb0JBQW9CLEVBQUMsTUFBTSxvQkFBb0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUV2RCxPQUFPLDRCQUE0QixDQUFDO0FBR3BDLE1BQU0sT0FBTyxhQUFhOzs7O0lBTXhCLFlBQW9CLFlBQWtDO1FBQWxDLGlCQUFZLEdBQVosWUFBWSxDQUFzQjtRQUx0RCxjQUFTLEdBQVksS0FBSyxDQUFDO1FBQzNCLGdCQUFXLEdBQVksSUFBSSxDQUFDO1FBSzFCLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUNoQixDQUFDOzs7O0lBQ0QsTUFBTTtRQUNKLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ25CLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQyxJQUFJOzs7WUFBQyxHQUFHLEVBQUU7Z0JBQzVCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO2dCQUN6QixPQUFPLENBQUMsR0FBRyxDQUFDLGlDQUFpQyxDQUFDLENBQUM7WUFDakQsQ0FBQyxFQUFDLENBQUM7U0FDSjtJQUNILENBQUM7Ozs7Ozs7SUFFRCxNQUFNLENBQUMsTUFBTSxFQUFFLE9BQU8sRUFBRSxNQUFNO1FBQzVCLE9BQU87WUFDTCxRQUFRLEVBQUUsT0FBTztZQUNqQixLQUFLLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxZQUFZO1lBQy9DLE9BQU8sRUFBRTtnQkFDUCxRQUFRLEVBQUUsVUFBVTthQUNyQjtZQUNELFFBQVEsRUFBRTtnQkFDUixTQUFTLEVBQUUsTUFBTSxDQUFDLFFBQVE7Z0JBQzFCLFlBQVksRUFBRSxNQUFNLENBQUMsUUFBUTthQUM5QjtZQUNELFFBQVEsRUFBRSxJQUFJO1lBQ2QsU0FBUzs7Ozs7WUFBRSxDQUFDLElBQUksRUFBRSxPQUFPLEVBQUUsRUFBRTtnQkFDM0IsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQztvQkFDNUIsU0FBUyxFQUFFO3dCQUNULGNBQWMsRUFBRTs0QkFDZDtnQ0FDRSxRQUFRLEVBQUU7b0NBQ1IsT0FBTyxFQUFFLE1BQU0sQ0FBQyxNQUFNO29DQUN0QixVQUFVLEVBQUUsTUFBTSxDQUFDLFFBQVE7aUNBQzVCO2dDQUNELGFBQWEsRUFBRSxNQUFNLENBQUMsV0FBVyxJQUFJLEVBQUU7NkJBQ3hDO3lCQUNGO3FCQUNGO2lCQUNGLENBQUMsQ0FBQztZQUNMLENBQUMsQ0FBQTtZQUNELFdBQVc7Ozs7O1lBQUUsQ0FBQyxJQUFJLEVBQUUsT0FBTyxFQUFFLEVBQUU7Z0JBQzdCLE9BQU8sT0FBTyxDQUFDLE9BQU87cUJBQ25CLE9BQU8sRUFBRTtxQkFDVCxJQUFJOzs7O2dCQUFDLE9BQU8sQ0FBQyxFQUFFOzt3QkFDVixjQUFjLEdBQUcsRUFBRTtvQkFDdkIsSUFDRSxPQUFPO3dCQUNQLE9BQU8sQ0FBQyxZQUFZO3dCQUNwQixPQUFPLENBQUMsWUFBWSxDQUFDLE1BQU07d0JBQzNCLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCO3dCQUN6QyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLE1BQU07d0JBQ2hELE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTt3QkFDakQsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUNwRDt3QkFDQSxjQUFjOzRCQUNaLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQztxQkFDeEQ7b0JBQ0QsK0NBQStDO29CQUMvQyxJQUFJLE9BQU8sQ0FBQyxNQUFNLElBQUksTUFBTSxJQUFJLE9BQU8sQ0FBQyxLQUFLLElBQUksVUFBVSxFQUFFO3dCQUMzRCxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7cUJBQ2pCO3lCQUFNO3dCQUNMLE9BQU8sQ0FBQzs0QkFDTixZQUFZLEVBQUUsTUFBTSxDQUFDLE1BQU07NEJBQzNCLFVBQVUsRUFBRSxDQUFDOzRCQUNiLFVBQVUsRUFBRSxNQUFNLENBQUMsTUFBTTs0QkFDekIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxRQUFROzRCQUN6QixPQUFPLEVBQUUsY0FBYzt5QkFDeEIsQ0FBQyxDQUFDO3dCQUVILElBQUcsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBQzs0QkFDdEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQzt5QkFDdkI7cUJBQ0Y7Z0JBQ0gsQ0FBQyxFQUFDO3FCQUNELEtBQUs7Ozs7Z0JBQUMsQ0FBQyxDQUFDLEVBQUU7b0JBQ1QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDZixNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2YsQ0FBQyxFQUFDLENBQUM7WUFDUCxDQUFDLENBQUE7U0FDRixDQUFDO0lBQ0osQ0FBQzs7OztJQUVELFlBQVk7UUFDVixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN0QixPQUFPLElBQUksT0FBTzs7Ozs7UUFBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTs7Z0JBQ2pDLGdCQUFnQixHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDO1lBQ3ZELGdCQUFnQixDQUFDLEdBQUcsR0FBRywrQ0FBK0MsQ0FBQztZQUN2RSxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDO1lBQ2xDLGdCQUFnQixDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7WUFDbEMsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUM5QyxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsVUFBVSxDQUFDLE1BQVc7UUFDcEIsT0FBTyxJQUFJLE9BQU87Ozs7O1FBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7O2dCQUNqQyxZQUFZLEdBQVEsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQzs7Z0JBQzlDLFVBQVUsR0FBUSxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxHQUFHLFlBQVksR0FBRyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDOztnQkFDL0QsVUFBVSxHQUFRLENBQUMsWUFBWSxHQUFHLFVBQVUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDNUQsT0FBTyxDQUFDO2dCQUNOLE9BQU8sRUFBRSxLQUFLLEdBQUcsSUFBSSxFQUFFO2dCQUN2QixZQUFZLEVBQUUsWUFBWTtnQkFDMUIsVUFBVSxFQUFFLFVBQVU7Z0JBQ3RCLFVBQVUsRUFBRSxVQUFVO2dCQUN0QixRQUFRLEVBQUUsS0FBSzthQUNoQixDQUFDLENBQUM7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLE1BQVc7O1lBQ1osSUFBSSxHQUFRLElBQUk7UUFDcEIsTUFBTSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQztRQUNuQyxPQUFPLElBQUksT0FBTzs7Ozs7UUFBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUNyQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRTtnQkFDbkIsT0FBTyxDQUFDLEtBQUssQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFDO2dCQUN4RCxPQUFPLEtBQUssQ0FBQzthQUNkOztrQkFFSyxZQUFZLEdBQVE7Z0JBQ3hCLFNBQVMsRUFBRSxLQUFLO2dCQUNoQixXQUFXLEVBQUUsa0JBQWtCO2FBQ2hDO1lBRUQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsb0JBQW9CLEVBQUUsWUFBWSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDbkcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsU0FBUzs7OztZQUFDLENBQUMsTUFBTSxFQUFFLEVBQUU7Z0JBQ2hELElBQUksQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLEVBQUUsTUFBTSxFQUFFLE1BQU0sQ0FBQyxNQUFNLEVBQUUsRUFBRSxhQUFhLENBQUMsQ0FBQztZQUMzRSxDQUFDLEVBQUMsQ0FBQztZQUVILElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFNBQVM7Ozs7WUFBQyxDQUFDLE1BQU0sRUFBRSxFQUFFO2dCQUN6QyxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQTtZQUMvQixDQUFDLEVBQUMsQ0FBQztZQUVILE1BQU0sQ0FBQyxVQUFVOzs7WUFBQyxHQUFHLEVBQUU7Z0JBQ3JCLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FDekIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsT0FBTyxFQUFFLE1BQU0sQ0FBQyxFQUNwQywyQkFBMkIsQ0FDNUIsQ0FBQztZQUNKLENBQUMsR0FBRSxJQUFJLENBQUMsQ0FBQztRQUNYLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7O1lBaEpGLFVBQVU7Ozs7WUFKRixvQkFBb0I7Ozs7SUFNM0Isa0NBQTJCOztJQUMzQixvQ0FBNEI7O0lBQzVCLGdDQUFhOzs7OztJQUNiLGlDQUFzQjs7Ozs7SUFFVixxQ0FBMEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHt2NCBhcyB1dWlkfSBmcm9tICd1dWlkJztcbmltcG9ydCB7UGF5cGFsTW9kYWxDb21wb25lbnR9IGZyb20gJy4vcGF5cGFsLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBOZ3hTbWFydE1vZGFsU2VydmljZSB9IGZyb20gJ25neC1zbWFydC1tb2RhbCc7XG5cbmltcG9ydCAndXJsLXNlYXJjaC1wYXJhbXMtcG9seWZpbGwnO1xuZGVjbGFyZSB2YXIgd2luZG93OiBhbnk7XG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgUGF5UGFsU2VydmljZSB7XG4gIGFkZFNjcmlwdDogYm9vbGVhbiA9IGZhbHNlO1xuICBzY3JpcHRSZWFkeTogYm9vbGVhbiA9IHRydWU7XG4gIGhhbmRsZXI6IGFueTtcbiAgcHJpdmF0ZSBtb2RhbFJlZjogYW55O1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgbW9kYWxTZXJ2aWNlOiBOZ3hTbWFydE1vZGFsU2VydmljZSkge1xuICAgIHRoaXMuX19pbml0KCk7XG4gIH1cbiAgX19pbml0KCk6IHZvaWQge1xuICAgIGlmICghdGhpcy5hZGRTY3JpcHQpIHtcbiAgICAgIHRoaXMuaW5qZWN0U2NyaXB0KCkudGhlbigoKSA9PiB7XG4gICAgICAgIHRoaXMuc2NyaXB0UmVhZHkgPSBmYWxzZTtcbiAgICAgICAgY29uc29sZS5sb2coJ1BheVBhbCBjaGVja291dC5qcyBzY3JpcHQgcmVhZHknKTtcbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG4gIGNvbmZpZyhwYXJhbXMsIHJlc29sdmUsIHJlamVjdCkge1xuICAgIHJldHVybiB7XG4gICAgICBcImxvY2FsZVwiOiAnZW5fVVMnLFxuICAgICAgXCJlbnZcIjogcGFyYW1zLmlzVGVzdCA/ICdzYW5kYm94JyA6ICdwcm9kdWN0aW9uJyxcbiAgICAgIFwic3R5bGVcIjoge1xuICAgICAgICBcImxheW91dFwiOiAndmVydGljYWwnLFxuICAgICAgfSxcbiAgICAgIFwiY2xpZW50XCI6IHtcbiAgICAgICAgXCJzYW5kYm94XCI6IHBhcmFtcy51c2VybmFtZSxcbiAgICAgICAgXCJwcm9kdWN0aW9uXCI6IHBhcmFtcy51c2VybmFtZSxcbiAgICAgIH0sXG4gICAgICBcImNvbW1pdFwiOiB0cnVlLFxuICAgICAgXCJwYXltZW50XCI6IChkYXRhLCBhY3Rpb25zKSA9PiB7XG4gICAgICAgIHJldHVybiBhY3Rpb25zLnBheW1lbnQuY3JlYXRlKHtcbiAgICAgICAgICBcInBheW1lbnRcIjoge1xuICAgICAgICAgICAgXCJ0cmFuc2FjdGlvbnNcIjogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgXCJhbW91bnRcIjoge1xuICAgICAgICAgICAgICAgICAgXCJ0b3RhbFwiOiBwYXJhbXMuYW1vdW50LCBcbiAgICAgICAgICAgICAgICAgIFwiY3VycmVuY3lcIjogcGFyYW1zLmN1cnJlbmN5XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBcImRlc2NyaXB0aW9uXCI6IHBhcmFtcy5kZXNjcmlwdGlvbiB8fCAnJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgXSxcbiAgICAgICAgICB9LFxuICAgICAgICB9KTtcbiAgICAgIH0sXG4gICAgICBvbkF1dGhvcml6ZTogKGRhdGEsIGFjdGlvbnMpID0+IHtcbiAgICAgICAgcmV0dXJuIGFjdGlvbnMucGF5bWVudFxuICAgICAgICAgIC5leGVjdXRlKClcbiAgICAgICAgICAudGhlbihwYXltZW50ID0+IHtcbiAgICAgICAgICAgIHZhciB0cmFuc2FjdGlvbl9pZCA9ICcnO1xuICAgICAgICAgICAgaWYgKFxuICAgICAgICAgICAgICBwYXltZW50ICYmXG4gICAgICAgICAgICAgIHBheW1lbnQudHJhbnNhY3Rpb25zICYmXG4gICAgICAgICAgICAgIHBheW1lbnQudHJhbnNhY3Rpb25zLmxlbmd0aCAmJlxuICAgICAgICAgICAgICBwYXltZW50LnRyYW5zYWN0aW9uc1swXS5yZWxhdGVkX3Jlc291cmNlcyAmJlxuICAgICAgICAgICAgICBwYXltZW50LnRyYW5zYWN0aW9uc1swXS5yZWxhdGVkX3Jlc291cmNlcy5sZW5ndGggJiZcbiAgICAgICAgICAgICAgcGF5bWVudC50cmFuc2FjdGlvbnNbMF0ucmVsYXRlZF9yZXNvdXJjZXNbMF0uc2FsZSAmJlxuICAgICAgICAgICAgICBwYXltZW50LnRyYW5zYWN0aW9uc1swXS5yZWxhdGVkX3Jlc291cmNlc1swXS5zYWxlLmlkXG4gICAgICAgICAgICApIHtcbiAgICAgICAgICAgICAgdHJhbnNhY3Rpb25faWQgPVxuICAgICAgICAgICAgICAgIHBheW1lbnQudHJhbnNhY3Rpb25zWzBdLnJlbGF0ZWRfcmVzb3VyY2VzWzBdLnNhbGUuaWQ7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyBjaGVjayBpZiB0aGUgUGF5UGFsIHRyYW5zYWN0aW9uIHdlbnQgdGhyb3VnaFxuICAgICAgICAgICAgaWYgKHBheW1lbnQuaW50ZW50ICE9ICdzYWxlJyAmJiBwYXltZW50LnN0YXRlICE9ICdhcHByb3ZlZCcpIHtcbiAgICAgICAgICAgICAgcmVqZWN0KHBheW1lbnQpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgcmVzb2x2ZSh7XG4gICAgICAgICAgICAgICAgdG90YWxfYW1vdW50OiBwYXJhbXMuYW1vdW50LFxuICAgICAgICAgICAgICAgIGZlZV9hbW91bnQ6IDAsXG4gICAgICAgICAgICAgICAgbmV0X2Ftb3VudDogcGFyYW1zLmFtb3VudCxcbiAgICAgICAgICAgICAgICBjdXJyZW5jeTogcGFyYW1zLmN1cnJlbmN5LFxuICAgICAgICAgICAgICAgIHRyeG5faWQ6IHRyYW5zYWN0aW9uX2lkLFxuICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICBpZih0aGlzLm1vZGFsUmVmICYmIHRoaXMubW9kYWxSZWYuY2xvc2Upe1xuICAgICAgICAgICAgICAgIHRoaXMubW9kYWxSZWYuY2xvc2UoKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH0pXG4gICAgICAgICAgLmNhdGNoKGUgPT4ge1xuICAgICAgICAgICAgY29uc29sZS5sb2coZSk7XG4gICAgICAgICAgICByZWplY3QobnVsbCk7XG4gICAgICAgICAgfSk7XG4gICAgICB9LFxuICAgIH07XG4gIH1cblxuICBpbmplY3RTY3JpcHQoKSB7XG4gICAgdGhpcy5hZGRTY3JpcHQgPSB0cnVlO1xuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBsZXQgc2NyaXB0dGFnRWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NjcmlwdCcpO1xuICAgICAgc2NyaXB0dGFnRWxlbWVudC5zcmMgPSAnaHR0cHM6Ly93d3cucGF5cGFsb2JqZWN0cy5jb20vYXBpL2NoZWNrb3V0LmpzJztcbiAgICAgIHNjcmlwdHRhZ0VsZW1lbnQub25sb2FkID0gcmVzb2x2ZTtcbiAgICAgIHNjcmlwdHRhZ0VsZW1lbnQub25lcnJvciA9IHJlamVjdDtcbiAgICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQoc2NyaXB0dGFnRWxlbWVudCk7XG4gICAgfSk7XG4gIH1cblxuICBmYWtlQ2hhcmdlKHBhcmFtczogYW55KSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIGxldCB0b3RhbF9hbW91bnQ6IGFueSA9IHBhcnNlSW50KHBhcmFtc1snYW1vdW50J10pO1xuICAgICAgbGV0IGZlZV9hbW91bnQ6IGFueSA9ICgoMTAwICogMi45KSAvIHRvdGFsX2Ftb3VudCArIDAuMykudG9GaXhlZCgyKTtcbiAgICAgIGxldCBuZXRfYW1vdW50OiBhbnkgPSAodG90YWxfYW1vdW50IC0gZmVlX2Ftb3VudCkudG9GaXhlZCgyKTtcbiAgICAgIHJlc29sdmUoe1xuICAgICAgICB0cnhuX2lkOiAnY2hfJyArIHV1aWQoKSxcbiAgICAgICAgdG90YWxfYW1vdW50OiB0b3RhbF9hbW91bnQsXG4gICAgICAgIGZlZV9hbW91bnQ6IGZlZV9hbW91bnQsXG4gICAgICAgIG5ldF9hbW91bnQ6IG5ldF9hbW91bnQsXG4gICAgICAgIGN1cnJlbmN5OiAndXNkJyxcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgY2hhcmdlKHBhcmFtczogYW55KSB7XG4gICAgbGV0IHNlbGY6IGFueSA9IHRoaXM7XG4gICAgcGFyYW1zLmFtb3VudCA9IHBhcmFtcy5hbW91bnQgfHwgMDtcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgaWYgKCF0aGlzLmFkZFNjcmlwdCkge1xuICAgICAgICBjb25zb2xlLmVycm9yKCdQYXlQYWwgY2hlY2tvdXQuanMgc2NyaXB0IGlzIG5vdCByZWFkeScpO1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG5cbiAgICAgIGNvbnN0IG1vZGFsT3B0aW9uczogYW55ID0ge1xuICAgICAgICBlc2NhcGFibGU6IGZhbHNlLFxuICAgICAgICBjdXN0b21DbGFzczogJ3BheXBhbE1vZGFsQ2xhc3MnXG4gICAgICB9O1xuXG4gICAgICB0aGlzLm1vZGFsUmVmID0gdGhpcy5tb2RhbFNlcnZpY2UuY3JlYXRlKCdQYXlwYWxNb2RhbCcsIFBheXBhbE1vZGFsQ29tcG9uZW50LCBtb2RhbE9wdGlvbnMpLm9wZW4oKTtcbiAgICAgIHRoaXMubW9kYWxSZWYub25PcGVuRmluaXNoZWQuc3Vic2NyaWJlKChyZXN1bHQpID0+IHtcbiAgICAgICAgdGhpcy5tb2RhbFNlcnZpY2Uuc2V0TW9kYWxEYXRhKHsgaXNUZXN0OiBwYXJhbXMuaXNUZXN0IH0sICdQYXlwYWxNb2RhbCcpO1xuICAgICAgfSk7XG4gICAgICBcbiAgICAgIHRoaXMubW9kYWxSZWYub25DbG9zZS5zdWJzY3JpYmUoKHJlc3VsdCkgPT4ge1xuICAgICAgICBjb25zb2xlLmxvZygnY2xvc2VkJywgcmVzdWx0KVxuICAgICAgfSk7XG4gICAgICBcbiAgICAgIHdpbmRvdy5zZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgd2luZG93LnBheXBhbC5CdXR0b24ucmVuZGVyKFxuICAgICAgICAgIHNlbGYuY29uZmlnKHBhcmFtcywgcmVzb2x2ZSwgcmVqZWN0KSxcbiAgICAgICAgICAnI3BheXBhbC1idXR0b25zLWNvbnRhaW5lcicsXG4gICAgICAgICk7XG4gICAgICB9LCAxMDAwKTtcbiAgICB9KTtcbiAgfVxufSJdfQ==