/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { v4 as uuid } from 'uuid';
import 'url-search-params-polyfill';
export class StripeService {
    /**
     * @param {?} http
     */
    constructor(http) {
        this.http = http;
        this.addScript = false;
        this.scriptReady = true;
        this.__init();
    }
    /**
     * @return {?}
     */
    __init() {
        if (!this.addScript) {
            this.injectScript().then((/**
             * @return {?}
             */
            () => {
                this.scriptReady = false;
                console.log('stripe.js script ready', window.StripeCheckout);
            }));
        }
    }
    /**
     * @return {?}
     */
    injectScript() {
        this.addScript = true;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            /** @type {?} */
            let scripttagElement = document.createElement('script');
            scripttagElement.src = 'https://checkout.stripe.com/checkout.js';
            scripttagElement.onload = resolve;
            scripttagElement.onerror = reject;
            document.body.appendChild(scripttagElement);
        }));
    }
    /**
     * @param {?} params
     * @return {?}
     */
    fakeCharge(params) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            /** @type {?} */
            let total_amount = parseInt(params["amount"]);
            /** @type {?} */
            let fee_amount = (((100 * 2.9) / total_amount) + 0.30).toFixed(2);
            /** @type {?} */
            let net_amount = (total_amount - fee_amount).toFixed(2);
            resolve({
                "trxn_id": "ch_" + uuid(),
                "total_amount": total_amount,
                "fee_amount": fee_amount,
                "net_amount": net_amount,
                "currency": "usd"
            });
        }));
    }
    /**
     * @param {?} params
     * @return {?}
     */
    charge(params) {
        /** @type {?} */
        let self = this;
        params.amount = (params.amount || 0) * 100;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            self.handler = window.StripeCheckout.configure({
                key: params.username,
                locale: 'auto',
                name: params.name || '',
                image: params.image || '',
                billingAddress: false,
                zipCode: false,
                allowRememberMe: false,
                description: params.description || '',
                closed: (/**
                 * @param {?} e
                 * @return {?}
                 */
                function (e) {
                    if (!self.handler.isTokenGenerate) {
                        reject({
                            "code": "stripe_transaction_cancelled"
                        });
                    }
                }),
                token: (/**
                 * @param {?} token
                 * @return {?}
                 */
                function (token) {
                    resolve({
                        "total_amount": (params.amount / 100),
                        "fee_amount": 0,
                        "net_amount": (params.amount / 100),
                        "currency": params.currency,
                        "trxn_id": token.id
                    });
                    return false;
                })
            });
            self.handler.open(params);
        }));
    }
    /**
     * @return {?}
     */
    onPopstate() {
        this.handler.close();
    }
}
StripeService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
StripeService.ctorParameters = () => [
    { type: HttpClient }
];
StripeService.propDecorators = {
    onPopstate: [{ type: HostListener, args: ['window:popstate',] }]
};
if (false) {
    /** @type {?} */
    StripeService.prototype.addScript;
    /** @type {?} */
    StripeService.prototype.scriptReady;
    /** @type {?} */
    StripeService.prototype.handler;
    /**
     * @type {?}
     * @private
     */
    StripeService.prototype.http;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RyaXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vcGF5bWVudGpzLyIsInNvdXJjZXMiOlsidmVuZG9yL3N0cmlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekQsT0FBTyxFQUFFLFVBQVUsRUFBZSxNQUFNLHNCQUFzQixDQUFDO0FBQy9ELE9BQU8sRUFBRSxFQUFFLElBQUksSUFBSSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2xDLE9BQU8sNEJBQTRCLENBQUE7QUFJbkMsTUFBTSxPQUFPLGFBQWE7Ozs7SUFJeEIsWUFBb0IsSUFBZ0I7UUFBaEIsU0FBSSxHQUFKLElBQUksQ0FBWTtRQUhwQyxjQUFTLEdBQVksS0FBSyxDQUFDO1FBQzNCLGdCQUFXLEdBQVksSUFBSSxDQUFDO1FBRVksSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQUMsQ0FBQzs7OztJQUV4RCxNQUFNO1FBQ0osSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDbkIsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLElBQUk7OztZQUFDLEdBQUcsRUFBRTtnQkFDNUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7Z0JBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLEVBQUUsTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQy9ELENBQUMsRUFBQyxDQUFBO1NBQ0g7SUFDSCxDQUFDOzs7O0lBRUQsWUFBWTtRQUNWLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLE9BQU8sSUFBSSxPQUFPOzs7OztRQUFDLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFOztnQkFDakMsZ0JBQWdCLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUM7WUFDdkQsZ0JBQWdCLENBQUMsR0FBRyxHQUFHLHlDQUF5QyxDQUFDO1lBQ2pFLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUM7WUFDbEMsZ0JBQWdCLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztZQUNsQyxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQzlDLENBQUMsRUFBQyxDQUFBO0lBQ0osQ0FBQzs7Ozs7SUFFRCxVQUFVLENBQUMsTUFBVztRQUNwQixPQUFPLElBQUksT0FBTzs7Ozs7UUFBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTs7Z0JBQ2pDLFlBQVksR0FBTyxRQUFRLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDOztnQkFDN0MsVUFBVSxHQUFPLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsR0FBRyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDOztnQkFDakUsVUFBVSxHQUFPLENBQUMsWUFBWSxHQUFHLFVBQVUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFFM0QsT0FBTyxDQUFDO2dCQUNOLFNBQVMsRUFBRSxLQUFLLEdBQUcsSUFBSSxFQUFFO2dCQUN6QixjQUFjLEVBQUUsWUFBWTtnQkFDNUIsWUFBWSxFQUFFLFVBQVU7Z0JBQ3hCLFlBQVksRUFBRSxVQUFVO2dCQUN4QixVQUFVLEVBQUUsS0FBSzthQUNsQixDQUFDLENBQUM7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLE1BQVc7O1lBQ1osSUFBSSxHQUFRLElBQUk7UUFDcEIsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO1FBQzNDLE9BQU8sSUFBSSxPQUFPOzs7OztRQUFDLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQ3JDLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUM7Z0JBQzdDLEdBQUcsRUFBRSxNQUFNLENBQUMsUUFBUTtnQkFDcEIsTUFBTSxFQUFFLE1BQU07Z0JBQ2QsSUFBSSxFQUFFLE1BQU0sQ0FBQyxJQUFJLElBQUksRUFBRTtnQkFDdkIsS0FBSyxFQUFFLE1BQU0sQ0FBQyxLQUFLLElBQUksRUFBRTtnQkFDekIsY0FBYyxFQUFFLEtBQUs7Z0JBQ3JCLE9BQU8sRUFBRSxLQUFLO2dCQUNkLGVBQWUsRUFBRSxLQUFLO2dCQUN0QixXQUFXLEVBQUUsTUFBTSxDQUFDLFdBQVcsSUFBSSxFQUFFO2dCQUNyQyxNQUFNOzs7O2dCQUFFLFVBQVUsQ0FBQztvQkFDakIsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsZUFBZSxFQUFFO3dCQUNqQyxNQUFNLENBQUM7NEJBQ0wsTUFBTSxFQUFFLDhCQUE4Qjt5QkFDdkMsQ0FBQyxDQUFDO3FCQUNKO2dCQUNILENBQUMsQ0FBQTtnQkFDRCxLQUFLOzs7O2dCQUFFLFVBQVMsS0FBSztvQkFDbkIsT0FBTyxDQUFDO3dCQUNOLGNBQWMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDO3dCQUNyQyxZQUFZLEVBQUMsQ0FBQzt3QkFDZCxZQUFZLEVBQUUsQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQzt3QkFDbkMsVUFBVSxFQUFFLE1BQU0sQ0FBQyxRQUFRO3dCQUMzQixTQUFTLEVBQUUsS0FBSyxDQUFDLEVBQUU7cUJBQ3BCLENBQUMsQ0FBQztvQkFDSCxPQUFPLEtBQUssQ0FBQztnQkFDZixDQUFDLENBQUE7YUFDRixDQUFDLENBQUM7WUFFSCxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM1QixDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7SUFHRCxVQUFVO1FBQ1IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQTtJQUN0QixDQUFDOzs7WUFsRkYsVUFBVTs7OztZQUxGLFVBQVU7Ozt5QkFvRmhCLFlBQVksU0FBQyxpQkFBaUI7Ozs7SUE3RS9CLGtDQUEyQjs7SUFDM0Isb0NBQTRCOztJQUM1QixnQ0FBYTs7Ozs7SUFDRCw2QkFBd0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBIb3N0TGlzdGVuZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBIZWFkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgdjQgYXMgdXVpZCB9IGZyb20gJ3V1aWQnO1xuaW1wb3J0ICd1cmwtc2VhcmNoLXBhcmFtcy1wb2x5ZmlsbCdcbmRlY2xhcmUgdmFyIHdpbmRvdzogYW55O1xu4oCLXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgU3RyaXBlU2VydmljZSB7XG4gIGFkZFNjcmlwdDogYm9vbGVhbiA9IGZhbHNlO1xuICBzY3JpcHRSZWFkeTogYm9vbGVhbiA9IHRydWU7XG4gIGhhbmRsZXI6IGFueTtcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50KSB7IHRoaXMuX19pbml0KCk7IH1cbuKAi1xuICBfX2luaXQoKTogdm9pZCB7XG4gICAgaWYgKCF0aGlzLmFkZFNjcmlwdCkge1xuICAgICAgdGhpcy5pbmplY3RTY3JpcHQoKS50aGVuKCgpID0+IHtcbiAgICAgICAgdGhpcy5zY3JpcHRSZWFkeSA9IGZhbHNlO1xuICAgICAgICBjb25zb2xlLmxvZygnc3RyaXBlLmpzIHNjcmlwdCByZWFkeScsIHdpbmRvdy5TdHJpcGVDaGVja291dCk7XG4gICAgICB9KVxuICAgIH1cbiAgfVxuXG4gIGluamVjdFNjcmlwdCgpIHtcbiAgICB0aGlzLmFkZFNjcmlwdCA9IHRydWU7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIGxldCBzY3JpcHR0YWdFbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc2NyaXB0Jyk7XG4gICAgICBzY3JpcHR0YWdFbGVtZW50LnNyYyA9ICdodHRwczovL2NoZWNrb3V0LnN0cmlwZS5jb20vY2hlY2tvdXQuanMnO1xuICAgICAgc2NyaXB0dGFnRWxlbWVudC5vbmxvYWQgPSByZXNvbHZlO1xuICAgICAgc2NyaXB0dGFnRWxlbWVudC5vbmVycm9yID0gcmVqZWN0O1xuICAgICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChzY3JpcHR0YWdFbGVtZW50KTtcbiAgICB9KVxuICB9XG5cbiAgZmFrZUNoYXJnZShwYXJhbXM6IGFueSkge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBsZXQgdG90YWxfYW1vdW50OmFueSA9IHBhcnNlSW50KHBhcmFtc1tcImFtb3VudFwiXSk7XG4gICAgICBsZXQgZmVlX2Ftb3VudDphbnkgPSAoKCgxMDAgKiAyLjkpIC8gdG90YWxfYW1vdW50KSArIDAuMzApLnRvRml4ZWQoMik7XG4gICAgICBsZXQgbmV0X2Ftb3VudDphbnkgPSAodG90YWxfYW1vdW50IC0gZmVlX2Ftb3VudCkudG9GaXhlZCgyKTtcbuKAi1xuICAgICAgcmVzb2x2ZSh7XG4gICAgICAgIFwidHJ4bl9pZFwiOiBcImNoX1wiICsgdXVpZCgpLFxuICAgICAgICBcInRvdGFsX2Ftb3VudFwiOiB0b3RhbF9hbW91bnQsXG4gICAgICAgIFwiZmVlX2Ftb3VudFwiOiBmZWVfYW1vdW50LFxuICAgICAgICBcIm5ldF9hbW91bnRcIjogbmV0X2Ftb3VudCxcbiAgICAgICAgXCJjdXJyZW5jeVwiOiBcInVzZFwiXG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxu4oCLXG4gIGNoYXJnZShwYXJhbXM6IGFueSkge1xuICAgIGxldCBzZWxmOiBhbnkgPSB0aGlzO1xuICAgIHBhcmFtcy5hbW91bnQgPSAocGFyYW1zLmFtb3VudCB8fCAwKSAqIDEwMDtcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgc2VsZi5oYW5kbGVyID0gd2luZG93LlN0cmlwZUNoZWNrb3V0LmNvbmZpZ3VyZSh7XG4gICAgICAgIGtleTogcGFyYW1zLnVzZXJuYW1lLFxuICAgICAgICBsb2NhbGU6ICdhdXRvJyxcbiAgICAgICAgbmFtZTogcGFyYW1zLm5hbWUgfHwgJycsXG4gICAgICAgIGltYWdlOiBwYXJhbXMuaW1hZ2UgfHwgJycsXG4gICAgICAgIGJpbGxpbmdBZGRyZXNzOiBmYWxzZSxcbiAgICAgICAgemlwQ29kZTogZmFsc2UsXG4gICAgICAgIGFsbG93UmVtZW1iZXJNZTogZmFsc2UsXG4gICAgICAgIGRlc2NyaXB0aW9uOiBwYXJhbXMuZGVzY3JpcHRpb24gfHwgJycsXG4gICAgICAgIGNsb3NlZDogZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICBpZiAoIXNlbGYuaGFuZGxlci5pc1Rva2VuR2VuZXJhdGUpIHtcbiAgICAgICAgICAgIHJlamVjdCh7XG4gICAgICAgICAgICAgIFwiY29kZVwiOiBcInN0cmlwZV90cmFuc2FjdGlvbl9jYW5jZWxsZWRcIlxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICB0b2tlbjogZnVuY3Rpb24odG9rZW4pIHtcbiAgICAgICAgICByZXNvbHZlKHtcbiAgICAgICAgICAgIFwidG90YWxfYW1vdW50XCI6IChwYXJhbXMuYW1vdW50IC8gMTAwKSxcbiAgICAgICAgICAgIFwiZmVlX2Ftb3VudFwiOjAsXG4gICAgICAgICAgICBcIm5ldF9hbW91bnRcIjogKHBhcmFtcy5hbW91bnQgLyAxMDApLFxuICAgICAgICAgICAgXCJjdXJyZW5jeVwiOiBwYXJhbXMuY3VycmVuY3ksXG4gICAgICAgICAgICBcInRyeG5faWRcIjogdG9rZW4uaWRcbiAgICAgICAgICB9KTtcbiAgICAgICAgICByZXR1cm4gZmFsc2U7ICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICB9KTtcbuKAi1xuICAgICAgc2VsZi5oYW5kbGVyLm9wZW4ocGFyYW1zKTtcbiAgICB9KTtcbiAgfVxu4oCLXG4gIEBIb3N0TGlzdGVuZXIoJ3dpbmRvdzpwb3BzdGF0ZScpXG4gIG9uUG9wc3RhdGUoKSB7XG4gICAgdGhpcy5oYW5kbGVyLmNsb3NlKClcbiAgfVxufSJdfQ==