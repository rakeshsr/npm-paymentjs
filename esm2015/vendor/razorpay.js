/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, HostListener } from '@angular/core';
import { v4 as uuid } from 'uuid';
import 'url-search-params-polyfill';
export class RazorpayService {
    constructor() {
        this.addScript = false;
        this.scriptReady = true;
        this.__init();
    }
    /**
     * @return {?}
     */
    __init() {
        if (!this.addScript) {
            this.injectScript().then((/**
             * @return {?}
             */
            () => {
                this.scriptReady = false;
                console.log('razorpay.js script ready', window.Razorpay);
            }));
        }
    }
    /**
     * @return {?}
     */
    injectScript() {
        this.addScript = true;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            /** @type {?} */
            let scripttagElement = document.createElement('script');
            scripttagElement.src = 'https://checkout.razorpay.com/v1/checkout.js';
            scripttagElement.onload = resolve;
            scripttagElement.onerror = reject;
            document.body.appendChild(scripttagElement);
        }));
    }
    /**
     * @param {?} params
     * @return {?}
     */
    fakeCharge(params) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            /** @type {?} */
            let total_amount = parseInt(params["amount"]);
            /** @type {?} */
            let fee_amount = (((100 * 2.9) / total_amount) + 0.30).toFixed(2);
            /** @type {?} */
            let net_amount = (total_amount - fee_amount).toFixed(2);
            resolve({
                "trxn_id": "ch_" + uuid(),
                "total_amount": total_amount,
                "fee_amount": fee_amount,
                "net_amount": net_amount,
                "currency": "usd"
            });
        }));
    }
    /**
     * @param {?} params
     * @return {?}
     */
    charge(params) {
        /** @type {?} */
        let self = this;
        /** @type {?} */
        const amount = (params.amount || 0) * 100;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            /** @type {?} */
            let options = {
                "key": params.username,
                "amount": amount,
                "currency": params.currency || "INR",
                "name": params.name || '',
                "description": params.description,
                "image": params.image || '',
                "prefill": {
                    "email": params.email || '',
                    "contact": params.phonenumber || ''
                },
                "modal": {
                    "ondismiss": (/**
                     * @return {?}
                     */
                    function () {
                        reject('Razorpay modal closed');
                    })
                },
                handler: (/**
                 * @param {?} token
                 * @return {?}
                 */
                function (token) {
                    resolve({
                        "total_amount": params.amount,
                        "fee_amount": 0,
                        "net_amount": params.amount,
                        "currency": params.currency,
                        "trxn_id": token.razorpay_payment_id,
                        "raw_response": token
                    });
                    return false;
                })
            }
            // optional
            ;
            // optional
            if (params.order_id) {
                options.order_id = params.order_id;
            }
            self.handler = new window.Razorpay(options);
            self.handler.open();
        }));
    }
    /**
     * @return {?}
     */
    onPopstate() {
        this.handler.close();
    }
}
RazorpayService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
RazorpayService.ctorParameters = () => [];
RazorpayService.propDecorators = {
    onPopstate: [{ type: HostListener, args: ['window:popstate',] }]
};
if (false) {
    /** @type {?} */
    RazorpayService.prototype.addScript;
    /** @type {?} */
    RazorpayService.prototype.scriptReady;
    /** @type {?} */
    RazorpayService.prototype.handler;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmF6b3JwYXkuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9wYXltZW50anMvIiwic291cmNlcyI6WyJ2ZW5kb3IvcmF6b3JwYXkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pELE9BQU8sRUFBRSxFQUFFLElBQUksSUFBSSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2xDLE9BQU8sNEJBQTRCLENBQUE7QUFJbkMsTUFBTSxPQUFPLGVBQWU7SUFLMUI7UUFKQSxjQUFTLEdBQVksS0FBSyxDQUFDO1FBQzNCLGdCQUFXLEdBQVksSUFBSSxDQUFDO1FBR1osSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQUMsQ0FBQzs7OztJQUVoQyxNQUFNO1FBQ0osSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDbkIsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLElBQUk7OztZQUFDLEdBQUcsRUFBRTtnQkFDNUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7Z0JBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsMEJBQTBCLEVBQUUsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzNELENBQUMsRUFBQyxDQUFBO1NBQ0g7SUFDSCxDQUFDOzs7O0lBRUQsWUFBWTtRQUNWLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLE9BQU8sSUFBSSxPQUFPOzs7OztRQUFDLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFOztnQkFDakMsZ0JBQWdCLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUM7WUFDdkQsZ0JBQWdCLENBQUMsR0FBRyxHQUFHLDhDQUE4QyxDQUFDO1lBQ3RFLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUM7WUFDbEMsZ0JBQWdCLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztZQUNsQyxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQzlDLENBQUMsRUFBQyxDQUFBO0lBQ0osQ0FBQzs7Ozs7SUFFRCxVQUFVLENBQUMsTUFBVztRQUNwQixPQUFPLElBQUksT0FBTzs7Ozs7UUFBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTs7Z0JBQ2pDLFlBQVksR0FBTyxRQUFRLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDOztnQkFDN0MsVUFBVSxHQUFPLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsR0FBRyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDOztnQkFDakUsVUFBVSxHQUFPLENBQUMsWUFBWSxHQUFHLFVBQVUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFFM0QsT0FBTyxDQUFDO2dCQUNOLFNBQVMsRUFBRSxLQUFLLEdBQUcsSUFBSSxFQUFFO2dCQUN6QixjQUFjLEVBQUUsWUFBWTtnQkFDNUIsWUFBWSxFQUFFLFVBQVU7Z0JBQ3hCLFlBQVksRUFBRSxVQUFVO2dCQUN4QixVQUFVLEVBQUUsS0FBSzthQUNsQixDQUFDLENBQUM7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLE1BQVc7O1lBQ1osSUFBSSxHQUFRLElBQUk7O2NBQ2QsTUFBTSxHQUFHLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsR0FBRyxHQUFHO1FBRXpDLE9BQU8sSUFBSSxPQUFPOzs7OztRQUFDLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFOztnQkFDakMsT0FBTyxHQUFRO2dCQUNqQixLQUFLLEVBQUUsTUFBTSxDQUFDLFFBQVE7Z0JBQ3RCLFFBQVEsRUFBRSxNQUFNO2dCQUNoQixVQUFVLEVBQUUsTUFBTSxDQUFDLFFBQVEsSUFBSSxLQUFLO2dCQUNwQyxNQUFNLEVBQUUsTUFBTSxDQUFDLElBQUksSUFBSSxFQUFFO2dCQUN6QixhQUFhLEVBQUUsTUFBTSxDQUFDLFdBQVc7Z0JBQ2pDLE9BQU8sRUFBRSxNQUFNLENBQUMsS0FBSyxJQUFJLEVBQUU7Z0JBQzNCLFNBQVMsRUFBRTtvQkFDVCxPQUFPLEVBQUUsTUFBTSxDQUFDLEtBQUssSUFBSSxFQUFFO29CQUMzQixTQUFTLEVBQUUsTUFBTSxDQUFDLFdBQVcsSUFBSSxFQUFFO2lCQUNwQztnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsV0FBVzs7O29CQUFFO3dCQUNYLE1BQU0sQ0FBQyx1QkFBdUIsQ0FBQyxDQUFBO29CQUNqQyxDQUFDLENBQUE7aUJBQ0Y7Z0JBQ0QsT0FBTzs7OztnQkFBRSxVQUFTLEtBQUs7b0JBQ3JCLE9BQU8sQ0FBQzt3QkFDTixjQUFjLEVBQUUsTUFBTSxDQUFDLE1BQU07d0JBQzdCLFlBQVksRUFBRSxDQUFDO3dCQUNmLFlBQVksRUFBRSxNQUFNLENBQUMsTUFBTTt3QkFDM0IsVUFBVSxFQUFFLE1BQU0sQ0FBQyxRQUFRO3dCQUMzQixTQUFTLEVBQUUsS0FBSyxDQUFDLG1CQUFtQjt3QkFDcEMsY0FBYyxFQUFFLEtBQUs7cUJBQ3RCLENBQUMsQ0FBQztvQkFDSCxPQUFPLEtBQUssQ0FBQztnQkFDZixDQUFDLENBQUE7YUFDRjtZQUVELFdBQVc7O1lBQVgsV0FBVztZQUNYLElBQUcsTUFBTSxDQUFDLFFBQVEsRUFBQztnQkFDakIsT0FBTyxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDO2FBQ3BDO1lBRUQsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLE1BQU0sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDNUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUN0QixDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7SUFHRCxVQUFVO1FBQ1IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQTtJQUN0QixDQUFDOzs7WUEzRkYsVUFBVTs7Ozs7eUJBd0ZSLFlBQVksU0FBQyxpQkFBaUI7Ozs7SUF0Ri9CLG9DQUEyQjs7SUFDM0Isc0NBQTRCOztJQUM1QixrQ0FBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIEhvc3RMaXN0ZW5lciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgdjQgYXMgdXVpZCB9IGZyb20gJ3V1aWQnO1xuaW1wb3J0ICd1cmwtc2VhcmNoLXBhcmFtcy1wb2x5ZmlsbCdcbmRlY2xhcmUgdmFyIHdpbmRvdzogYW55O1xu4oCLXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgUmF6b3JwYXlTZXJ2aWNlIHtcbiAgYWRkU2NyaXB0OiBib29sZWFuID0gZmFsc2U7XG4gIHNjcmlwdFJlYWR5OiBib29sZWFuID0gdHJ1ZTtcbiAgaGFuZGxlcjogYW55O1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB0aGlzLl9faW5pdCgpOyB9XG7igItcbiAgX19pbml0KCk6IHZvaWQge1xuICAgIGlmICghdGhpcy5hZGRTY3JpcHQpIHtcbiAgICAgIHRoaXMuaW5qZWN0U2NyaXB0KCkudGhlbigoKSA9PiB7XG4gICAgICAgIHRoaXMuc2NyaXB0UmVhZHkgPSBmYWxzZTtcbiAgICAgICAgY29uc29sZS5sb2coJ3Jhem9ycGF5LmpzIHNjcmlwdCByZWFkeScsIHdpbmRvdy5SYXpvcnBheSk7XG4gICAgICB9KVxuICAgIH1cbiAgfVxuXG4gIGluamVjdFNjcmlwdCgpIHtcbiAgICB0aGlzLmFkZFNjcmlwdCA9IHRydWU7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIGxldCBzY3JpcHR0YWdFbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc2NyaXB0Jyk7XG4gICAgICBzY3JpcHR0YWdFbGVtZW50LnNyYyA9ICdodHRwczovL2NoZWNrb3V0LnJhem9ycGF5LmNvbS92MS9jaGVja291dC5qcyc7XG4gICAgICBzY3JpcHR0YWdFbGVtZW50Lm9ubG9hZCA9IHJlc29sdmU7XG4gICAgICBzY3JpcHR0YWdFbGVtZW50Lm9uZXJyb3IgPSByZWplY3Q7XG4gICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKHNjcmlwdHRhZ0VsZW1lbnQpO1xuICAgIH0pXG4gIH1cblxuICBmYWtlQ2hhcmdlKHBhcmFtczogYW55KSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIGxldCB0b3RhbF9hbW91bnQ6YW55ID0gcGFyc2VJbnQocGFyYW1zW1wiYW1vdW50XCJdKTtcbiAgICAgIGxldCBmZWVfYW1vdW50OmFueSA9ICgoKDEwMCAqIDIuOSkgLyB0b3RhbF9hbW91bnQpICsgMC4zMCkudG9GaXhlZCgyKTtcbiAgICAgIGxldCBuZXRfYW1vdW50OmFueSA9ICh0b3RhbF9hbW91bnQgLSBmZWVfYW1vdW50KS50b0ZpeGVkKDIpO1xu4oCLXG4gICAgICByZXNvbHZlKHtcbiAgICAgICAgXCJ0cnhuX2lkXCI6IFwiY2hfXCIgKyB1dWlkKCksXG4gICAgICAgIFwidG90YWxfYW1vdW50XCI6IHRvdGFsX2Ftb3VudCxcbiAgICAgICAgXCJmZWVfYW1vdW50XCI6IGZlZV9hbW91bnQsXG4gICAgICAgIFwibmV0X2Ftb3VudFwiOiBuZXRfYW1vdW50LFxuICAgICAgICBcImN1cnJlbmN5XCI6IFwidXNkXCJcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG7igItcbiAgY2hhcmdlKHBhcmFtczogYW55KSB7XG4gICAgbGV0IHNlbGY6IGFueSA9IHRoaXM7XG4gICAgY29uc3QgYW1vdW50ID0gKHBhcmFtcy5hbW91bnQgfHwgMCkgKiAxMDBcblxuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBsZXQgb3B0aW9uczogYW55ID0ge1xuICAgICAgICBcImtleVwiOiBwYXJhbXMudXNlcm5hbWUsXG4gICAgICAgIFwiYW1vdW50XCI6IGFtb3VudCxcbiAgICAgICAgXCJjdXJyZW5jeVwiOiBwYXJhbXMuY3VycmVuY3kgfHwgXCJJTlJcIixcbiAgICAgICAgXCJuYW1lXCI6IHBhcmFtcy5uYW1lIHx8ICcnLFxuICAgICAgICBcImRlc2NyaXB0aW9uXCI6IHBhcmFtcy5kZXNjcmlwdGlvbixcbiAgICAgICAgXCJpbWFnZVwiOiBwYXJhbXMuaW1hZ2UgfHwgJycsXG4gICAgICAgIFwicHJlZmlsbFwiOiB7XG4gICAgICAgICAgXCJlbWFpbFwiOiBwYXJhbXMuZW1haWwgfHwgJycsXG4gICAgICAgICAgXCJjb250YWN0XCI6IHBhcmFtcy5waG9uZW51bWJlciB8fCAnJ1xuICAgICAgICB9LFxuICAgICAgICBcIm1vZGFsXCI6IHtcbiAgICAgICAgICBcIm9uZGlzbWlzc1wiOiBmdW5jdGlvbigpe1xuICAgICAgICAgICAgcmVqZWN0KCdSYXpvcnBheSBtb2RhbCBjbG9zZWQnKVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgaGFuZGxlcjogZnVuY3Rpb24odG9rZW4pIHtcbiAgICAgICAgICByZXNvbHZlKHtcbiAgICAgICAgICAgIFwidG90YWxfYW1vdW50XCI6IHBhcmFtcy5hbW91bnQsXG4gICAgICAgICAgICBcImZlZV9hbW91bnRcIjogMCxcbiAgICAgICAgICAgIFwibmV0X2Ftb3VudFwiOiBwYXJhbXMuYW1vdW50LFxuICAgICAgICAgICAgXCJjdXJyZW5jeVwiOiBwYXJhbXMuY3VycmVuY3ksXG4gICAgICAgICAgICBcInRyeG5faWRcIjogdG9rZW4ucmF6b3JwYXlfcGF5bWVudF9pZCxcbiAgICAgICAgICAgIFwicmF3X3Jlc3BvbnNlXCI6IHRva2VuICAgIFxuICAgICAgICAgIH0pO1xuICAgICAgICAgIHJldHVybiBmYWxzZTsgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgLy8gb3B0aW9uYWxcbiAgICAgIGlmKHBhcmFtcy5vcmRlcl9pZCl7XG4gICAgICAgIG9wdGlvbnMub3JkZXJfaWQgPSBwYXJhbXMub3JkZXJfaWQ7XG4gICAgICB9XG5cbiAgICAgIHNlbGYuaGFuZGxlciA9IG5ldyB3aW5kb3cuUmF6b3JwYXkob3B0aW9ucyk7XG4gICAgICBzZWxmLmhhbmRsZXIub3BlbigpO1xuICAgIH0pO1xuICB9XG7igItcbiAgQEhvc3RMaXN0ZW5lcignd2luZG93OnBvcHN0YXRlJylcbiAgb25Qb3BzdGF0ZSgpIHtcbiAgICB0aGlzLmhhbmRsZXIuY2xvc2UoKVxuICB9XG59Il19