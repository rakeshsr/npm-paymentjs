/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { StripeService } from '../vendor/stripe';
import { RazorpayService } from '../vendor/razorpay';
import { PayPalService } from '../vendor/paypal';
export class PaymentjsService {
    /**
     * @param {?} stripe
     * @param {?} razorpay
     * @param {?} paypal
     */
    constructor(stripe, razorpay, paypal) {
        this.stripe = stripe;
        this.razorpay = razorpay;
        this.paypal = paypal;
    }
    /**
     * @param {?} data
     * @return {?}
     */
    checkout(data) {
        /** @type {?} */
        let result;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const payment_type = data.paymentInstrument.toLowerCase();
            try {
                switch (payment_type) {
                    case 'payment_stripe':
                        result = yield this.stripe.charge(data.params);
                        break;
                    case 'payment_razorpay':
                        result = yield this.razorpay.charge(data.params);
                        break;
                    case 'payment_paypal':
                        result = yield this.paypal.charge(data.params);
                        break;
                    default:
                        reject(`Unsupported Payment Instrument ${data.paymentInstrument}`);
                        return false;
                        break;
                }
                result["isTest"] = data.params.isTest;
                result["paymentInstrument"] = data.paymentInstrument;
                resolve(result);
            }
            catch (e) {
                console.log(e);
                reject('Transaction cancelled!');
            }
        })));
    }
}
PaymentjsService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
PaymentjsService.ctorParameters = () => [
    { type: StripeService },
    { type: RazorpayService },
    { type: PayPalService }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    PaymentjsService.prototype.stripe;
    /**
     * @type {?}
     * @private
     */
    PaymentjsService.prototype.razorpay;
    /**
     * @type {?}
     * @private
     */
    PaymentjsService.prototype.paypal;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGF5bWVudGpzLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9wYXltZW50anMvIiwic291cmNlcyI6WyJsaWIvcGF5bWVudGpzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDckQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBR2pELE1BQU0sT0FBTyxnQkFBZ0I7Ozs7OztJQUMzQixZQUNVLE1BQXFCLEVBQ3JCLFFBQXlCLEVBQ3pCLE1BQXFCO1FBRnJCLFdBQU0sR0FBTixNQUFNLENBQWU7UUFDckIsYUFBUSxHQUFSLFFBQVEsQ0FBaUI7UUFDekIsV0FBTSxHQUFOLE1BQU0sQ0FBZTtJQUMzQixDQUFDOzs7OztJQUVMLFFBQVEsQ0FBQyxJQUFTOztZQUNaLE1BQVc7UUFDZixPQUFPLElBQUksT0FBTzs7Ozs7UUFBQyxDQUFNLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTs7a0JBQ3BDLFlBQVksR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxFQUFFO1lBQ3pELElBQUc7Z0JBQ0QsUUFBTyxZQUFZLEVBQUM7b0JBQ2xCLEtBQUssZ0JBQWdCO3dCQUNuQixNQUFNLEdBQUcsTUFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7d0JBQy9DLE1BQU07b0JBRVIsS0FBSyxrQkFBa0I7d0JBQ3JCLE1BQU0sR0FBRyxNQUFNLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDakQsTUFBTTtvQkFFUixLQUFLLGdCQUFnQjt3QkFDbkIsTUFBTSxHQUFHLE1BQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUMvQyxNQUFNO29CQUVSO3dCQUNFLE1BQU0sQ0FBQyxrQ0FBa0MsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUMsQ0FBQzt3QkFDbkUsT0FBTyxLQUFLLENBQUM7d0JBQ2IsTUFBTTtpQkFDVDtnQkFDRCxNQUFNLENBQUMsUUFBUSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUM7Z0JBQ3RDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztnQkFDckQsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ2pCO1lBQUEsT0FBTSxDQUFDLEVBQUM7Z0JBQ1AsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDZixNQUFNLENBQUMsd0JBQXdCLENBQUMsQ0FBQzthQUNsQztRQUNILENBQUMsQ0FBQSxFQUFDLENBQUE7SUFDSixDQUFDOzs7WUF2Q0YsVUFBVTs7OztZQUpGLGFBQWE7WUFDYixlQUFlO1lBQ2YsYUFBYTs7Ozs7OztJQUtsQixrQ0FBNkI7Ozs7O0lBQzdCLG9DQUFpQzs7Ozs7SUFDakMsa0NBQTZCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgU3RyaXBlU2VydmljZSB9IGZyb20gJy4uL3ZlbmRvci9zdHJpcGUnO1xuaW1wb3J0IHsgUmF6b3JwYXlTZXJ2aWNlIH0gZnJvbSAnLi4vdmVuZG9yL3Jhem9ycGF5JztcbmltcG9ydCB7IFBheVBhbFNlcnZpY2UgfSBmcm9tICcuLi92ZW5kb3IvcGF5cGFsJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFBheW1lbnRqc1NlcnZpY2Uge1xuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIHN0cmlwZTogU3RyaXBlU2VydmljZSxcbiAgICBwcml2YXRlIHJhem9ycGF5OiBSYXpvcnBheVNlcnZpY2UsXG4gICAgcHJpdmF0ZSBwYXlwYWw6IFBheVBhbFNlcnZpY2UsXG4gICkgeyB9XG4gIFxuICBjaGVja291dChkYXRhOiBhbnkpe1xuICAgIGxldCByZXN1bHQ6IGFueTtcbiAgICByZXR1cm4gbmV3IFByb21pc2UoYXN5bmMocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBjb25zdCBwYXltZW50X3R5cGUgPSBkYXRhLnBheW1lbnRJbnN0cnVtZW50LnRvTG93ZXJDYXNlKCk7XG4gICAgICB0cnl7XG4gICAgICAgIHN3aXRjaChwYXltZW50X3R5cGUpe1xuICAgICAgICAgIGNhc2UgJ3BheW1lbnRfc3RyaXBlJzpcbiAgICAgICAgICAgIHJlc3VsdCA9IGF3YWl0IHRoaXMuc3RyaXBlLmNoYXJnZShkYXRhLnBhcmFtcyk7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICBcbiAgICAgICAgICBjYXNlICdwYXltZW50X3Jhem9ycGF5JzpcbiAgICAgICAgICAgIHJlc3VsdCA9IGF3YWl0IHRoaXMucmF6b3JwYXkuY2hhcmdlKGRhdGEucGFyYW1zKTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgIFxuICAgICAgICAgIGNhc2UgJ3BheW1lbnRfcGF5cGFsJzpcbiAgICAgICAgICAgIHJlc3VsdCA9IGF3YWl0IHRoaXMucGF5cGFsLmNoYXJnZShkYXRhLnBhcmFtcyk7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICBcbiAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgcmVqZWN0KGBVbnN1cHBvcnRlZCBQYXltZW50IEluc3RydW1lbnQgJHtkYXRhLnBheW1lbnRJbnN0cnVtZW50fWApO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgICAgcmVzdWx0W1wiaXNUZXN0XCJdID0gZGF0YS5wYXJhbXMuaXNUZXN0O1xuICAgICAgICByZXN1bHRbXCJwYXltZW50SW5zdHJ1bWVudFwiXSA9IGRhdGEucGF5bWVudEluc3RydW1lbnQ7XG4gICAgICAgIHJlc29sdmUocmVzdWx0KTsgXG4gICAgICB9Y2F0Y2goZSl7XG4gICAgICAgIGNvbnNvbGUubG9nKGUpO1xuICAgICAgICByZWplY3QoJ1RyYW5zYWN0aW9uIGNhbmNlbGxlZCEnKTtcbiAgICAgIH1cbiAgICB9KSAgXG4gIH1cbn1cbiJdfQ==