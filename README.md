# Welcome to PaymentJS

Universal payment gateway client side integration library


# Supported Payment Gateways

 - PayPal
 - RazorPay
 - Stripe

## Installation

   app.module.ts
   
    import { PaymentjsModule } from  'paymentjs';

   app.component.ts
   
    import { PaymentjsService } from  'paymentjs';
    // ...
    constructor(private  payment: PaymentjsService) { }

## Checkout

    this.payment.checkout({
	  'paymentInstrument': Payment_Razorpay,
	  'params': {
	    'amount': 100,
	    'currency': 'USD',
	    'isTest': true,
	    'username': "RAZORPAY_USERNAME,
	    'email': 'raghava@inqude.com',
	  }
	}).then((r)=>{
	  console.log('result', r);
	}).catch((e)=>{
	  console.log('error', e);
	})

## Response

    {
	  "total_amount": 100,
	  "fee_amount": 0,
	  "net_amount": 100,
	  "currency": "USD",
	  "trxn_id": "pay_E2P4GrJLMrt7JS",
	  "isTest": true,
	  "paymentInstrument": "Payment_Razorpay"
	}
